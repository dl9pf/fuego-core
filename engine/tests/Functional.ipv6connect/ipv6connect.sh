tarball=ipv6connect.tar.gz

function test_build {
    make CC="$CC" LD="$LD" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put ipv6connect  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./ipv6connect"  
}

function test_processing {
	true
}

. $FUEGO_CORE/engine/scripts/functional.sh
