tarball=Dhrystone.tar.bz2

function test_pre_check {
    assert_define BENCHMARK_DHRYSTONE_LOOPS
}

function test_build {
    patch -p0 -N -s < $TEST_HOME/dhry_1.c.patch || return 1
    CFLAGS+=" -DTIME"
    LDFLAGS+=" -lm"
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" && touch test_suite_ready || return 1
}

function test_deploy {
    put dhrystone  $BOARD_TESTDIR/fuego.$TESTDIR/ || return 1
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./dhrystone $BENCHMARK_DHRYSTONE_LOOPS"
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
