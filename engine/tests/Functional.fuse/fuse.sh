tarball=fuse-2.9.4.tar.gz

function test_build {
    echo "fuse test build"
    patch -p1 -N < $TEST_HOME/patch_files/0001-fuse-fix-the-return-value-of-help-option.patch
    patch -p1 -N < $TEST_HOME/patch_files/aarch64.patch
    patch -p1 -N < $TEST_HOME/patch_files/gold-unversioned-symbol.patch
    ./configure --host=$PREFIX
    make
    tar cjf fuse_test_libs.tar.bz2 example/.libs
}

function test_deploy {
    put fuse_test_libs.tar.bz2 $BOARD_TESTDIR/fuego.$TESTDIR/;
    put $TEST_HOME/fuse_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/;
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v fuse_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "9" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh
