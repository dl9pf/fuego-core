# some functions are shared between Benchmark.netperf and Functional.netperf
tarball=../netperf/netperf-2.6.0.tar.bz2
source $FUEGO_CORE/engine/tests/netperf/netperf.sh

function test_pre_check {
    assert_define "BENCHMARK_NETPERF_SRV"
    if [ "$FUNCTIONAL_NETPERF_SRV" = "docker" ]; then
        check_process_is_running "netserver"
    fi
}

function test_deploy {
    put $TEST_HOME/netperf-rabench_script $BOARD_TESTDIR/fuego.$TESTDIR/
    put src/netperf  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    case "$BENCHMARK_NETPERF_SRV" in
        "default") srv=$SRV_IP ;;
        "docker") srv=$SRV_IP ;;
        *) srv=$BENCHMARK_NETPERF_SRV ;;
    esac
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./netperf-rabench_script $srv"
}

. $FUEGO_CORE/engine/scripts/benchmark.sh

