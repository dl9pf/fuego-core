#!/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib


ref_section_pat = "\[[\w]+.[gle]{2}\]"
cur_search_str = "^(Time:)(\ *)([\d]{1,8}.?[\d]{1,3})(.*)$"

cur_dict = {}
cur_file = open(plib.TEST_LOG,'r')
print "Reading current values from " + plib.TEST_LOG
cur_raw_values = cur_file.readlines()
cur_file.close()

for cur_item in cur_raw_values:
	cur_match = re.match(cur_search_str, cur_item)
	if cur_match:
		cur_dict["hackbench"] = cur_match.group(3)

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'sec'))
