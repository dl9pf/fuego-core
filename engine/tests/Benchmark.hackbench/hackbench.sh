tarball=hackbench.tar.gz

function test_build {
    $CC -lpthread hackbench.c -o hackbench && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put hackbench  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./hackbench $groups"  
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
