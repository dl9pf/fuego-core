tarball=bsdiff.tar.gz

function test_build {
    touch test_suite_ready
}

function test_deploy {
    put data/*.modified data/*.original $BOARD_TESTDIR/fuego.$TESTDIR/;
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
    if bsdiff 13.bspatch.original 13.bspatch.modified 13.diff any; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
    if bspatch 13.bspatch.original 13.out 13.diff; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
    if diff 13.bspatch.modified 13.out; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi"
}

function test_processing {
    log_compare "$TESTDIR" "3" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh
