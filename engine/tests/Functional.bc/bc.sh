#!/bin/bash

tarball=bc-script.tar.gz

function test_pre_check {
    assert_define FUNCTIONAL_BC_EXPR
    assert_define FUNCTIONAL_BC_RESULT
    is_on_target bc BC_PROGRAM /bin:/usr/bin:/usr/local/bin
    assert_define BC_PROGRAM
}

function test_build {
    echo "test compiling (is normally done here)"
}

function test_deploy {
    put bc-device.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./bc-device.sh $FUNCTIONAL_BC_EXPR $FUNCTIONAL_BC_RESULT"  
}

function test_processing {
    log_compare "$TESTDIR" "1" "OK" "p"          
}

. $FUEGO_CORE/engine/scripts/functional.sh
