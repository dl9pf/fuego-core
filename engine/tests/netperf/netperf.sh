function test_build {
    echo "ac_cv_func_setpgrp_void=yes" > config.cache
    ./configure --build=`./config.guess` --host=$HOST CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" --config-cache
    make && touch test_suite_ready || build_error "error while building test"
}

