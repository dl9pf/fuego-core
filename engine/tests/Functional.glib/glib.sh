tarball=glib-2.22.1.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/glib-strfuncs.patch
    # Don't run tests on the build host
    find . -name 'Makefile*' | xargs sed -i '/@test -z "${TEST_PROGS}" || ${GTESTER} --verbose ${TEST_PROGS}/d'
    echo -e "glib_cv_stack_grows=no \nglib_cv_uscore=no \nac_cv_func_posix_getpwuid_r=no \nac_cv_func_nonposix_getpwuid_r=no \nac_cv_func_posix_getgrgid_r=no" > config.cross
    ./configure --prefix=`pwd`/build --cache-file=config.cross --host=$HOST --target=$HOST --build=`uname -m`-linux-gnu CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CXXFLAGS="$CXXFLAGS"
    make 2>&1
    cd tests && make test; cd -
    cd glib/tests && make test; cd - && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	# Makefile.am is used by 'contexts' test. XXX - why?
	put gio/tests/Makefile.am `find . -name .libs | xargs -IDIRS find DIRS -not -type d -not -name '*.so*' -not -name '*.la*' -perm /u+x`  $BOARD_TESTDIR/fuego.$TESTDIR/
}

# Excluded tests
# --------------
# gtester - test runner
# echo-server - requires wrapper script
# errorcheck-mutex-test - requires wrapper script
# timeloop - hangs
# unicode-normalize - requires external data
# glib-genmarshal - requires external ???
# socket-server, socket-client - require wrapper script
# send-data - requires wrapper script
# testglib - ???
# testing - crashes
# gobject-query - requires external data
# desktop-app-info - requires supported desktop ???
# resolver - requires wrapper script
# unicode-collate - requires some locale data
# markup-subparser
# live-g-file
# filter-streams

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./gtester  ./objects ./readwrite ./g-file ./fileutils ./data-output-stream ./signal3 ./properties ./rand ./memory-input-stream ./sleepy-stream  ./g-icon ./array-test ./strfuncs ./testgdateparser ./srvtarget ./threadtests ./keyfile ./string ./hostutils ./memory-output-stream ./objects2 ./closures ./scannerapi ./testgdate ./data-input-stream ./signal2 ./signal1 ./testgobject ./g-file-info ./simple-async-result ./buffered-input-stream ./contexts ./printf ./properties2 ./unix-streams "  
}

function test_processing {
	#$SSH${DEVICE} "rm -rf $BOARD_TESTDIR/fuego.$TESTDIR"

	#log_compare "$TESTDIR" "196" "OK" "p"
	#log_compare "$TESTDIR" "0" "ERROR" "n"
	true
}

. $FUEGO_CORE/engine/scripts/functional.sh
