tarball=libwebsocket.tar.gz

function test_build {
    mkdir build
    cd build
    cmake -DOPENSSL_ROOT_DIR=$SDKTARGETSYSROOT/usr/lib -DOPENSSL_INCLUDE_DIR=$SDKTARGETSYSROOT/usr/include \
          -DOPENSSL_LIBRARIES=$SDKTARGETSYSROOT/usr/lib/ .. -DZLIB_LIBRARY=$SDKTARGETSYSROOT/usr/lib/libz.so -DZLIB_INCLUDE_DIR=$SDKTARGETSYSROOT/usr/include ..
    make
}

function test_deploy {	
	put build/bin/libwebsockets-test-server $BOARD_TESTDIR/fuego.$TESTDIR/
	put build/bin/libwebsockets-test-client $BOARD_TESTDIR/fuego.$TESTDIR/
	put build/bin/libwebsockets-test-echo $BOARD_TESTDIR/fuego.$TESTDIR/
	put build/bin/libwebsockets-test-ping $BOARD_TESTDIR/fuego.$TESTDIR/
	put build/bin/libwebsockets-test-fraggle $BOARD_TESTDIR/fuego.$TESTDIR/

	put libwebsockets-test-server.pem /usr/share
	put libwebsockets-test-server.key.pem /usr/share
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *;
	(./libwebsockets-test-server --resource_path=/usr/share &);
	if ./libwebsockets-test-client localhost ; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
	if ./libwebsockets-test-echo --client echo.websocket.org; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;\
	if ./libwebsockets-test-echo --client localhost --port 7681; then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;\
	if ./libwebsockets-test-ping localhost; then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi
        killall libwebsockets-test-server"
}

function test_processing {
	log_compare "$TESTDIR" "4" "^TEST.*OK" "p"
    	log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh


