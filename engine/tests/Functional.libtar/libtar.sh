function test_build {
    echo "#!/bin/bash
    rm -f testfile testfile.tar
    echo 'testfile' > testfile;
    libtar -C $BOARD_TESTDIR/fuego.$TESTDIR -c testfile.tar testfile;
    if [ -f testfile.tar ] ; then
    echo 'TEST-1 OK'
    else
    echo 'TEST-1 FAILED'
    fi
    rm -f testfile
    libtar -t testfile.tar | grep testfile
    if echo $ == 0 ; then
    echo 'TEST-2 OK'
    else
    echo 'TEST-2 FAILED'
    fi
    rm -f testfile
    libtar -x testfile.tar
    if [ -f testfile ] ; then
    echo 'TEST-3 OK'
    else
    echo 'TEST-3 FAILED'
    fi" > run-tests.sh
    touch test_suite_ready
}

function test_deploy {
    put run-tests.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v run-tests.sh 2>&1"
}

function test_processing {
    log_compare "$TESTDIR" "3" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh
