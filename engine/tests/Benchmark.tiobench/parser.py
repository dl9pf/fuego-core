#!/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib


ref_section_pat = "^\[[\w\d._]+.[gle]{2}\]"

cur_dict = {}
cur_file = open(plib.TEST_LOG,'r')
print "Reading current values from " + plib.TEST_LOG +"\n"
lines = cur_file.readlines()

cur_dict['Write.Synchronous'] = '%.2f' % float(lines[4].split('|')[3].lstrip(' ').rstrip(' MB/s'))
cur_dict['Write.Synchronous_Random'] = '%.2f' % float(lines[5].split('|')[3].lstrip(' ').rstrip(' MB/s'))
cur_dict['Read.Synchronous'] = '%.2f' % float(lines[6].split('|')[3].lstrip(' ').rstrip(' MB/s'))
cur_dict['Read.Synchronous_Random'] = '%.2f' % float(lines[7].split('|')[3].lstrip(' ').rstrip(' MB/s'))
cur_dict['Write.Sequential'] = '%.2f' % float(lines[25].split('|')[3].lstrip(' ').rstrip(' MB/s'))
cur_dict['Write.Sequential_Random'] = '%.2f' % float(lines[26].split('|')[3].lstrip(' ').rstrip(' MB/s'))
cur_dict['Read.Sequential'] = '%.2f' % float(lines[27].split('|')[3].lstrip(' ').rstrip(' MB/s'))
cur_dict['Read.Sequential_Random'] = '%.2f' % float(lines[28].split('|')[3].lstrip(' ').rstrip(' MB/s'))

cur_file.close()

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'Rate,MB/s'))
