tarball=Whetstone.tar.bz2

function test_build {
  	CFLAGS+=" -DTIME"
 	make CC="$CC" LD="$LD" LDFLAGS="$LDFLAGS" CFLAGS="$CFLAGS" LIBS=" -lm" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put whetstone  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_WHETSTONE_LOOPS
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR && ./whetstone $BENCHMARK_WHETSTONE_LOOPS"  
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
