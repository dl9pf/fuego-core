#!/usr/bin/python

import os, re, sys, time, collections

jta_home = os.environ.get('JTA_ENGINE_PATH', '/home/jenkins')
testname = os.environ.get('JOB_NAME', "Functional.xorg-macros")

ret_val = 0
parser_path = "%s/scripts/detailed_results/" % jta_home
sys.path.insert(0, parser_path)
import xmltodict

cur_dict = collections.OrderedDict()
cur_dict['report'] = collections.OrderedDict()
cur_dict['report']['name'] = testname

# Get build number
number_file = "%s/jta/jobs/%s/nextBuildNumber" % (jta_home, testname)
file_hd = open(number_file, 'r')
number_line = file_hd.read()
file_hd.close
number = int(number_line.strip()) - 1
number = int(os.environ.get('BUILD_NUMBER', number))

# Get start time and result of the testset
build_xml = "%s/jta/jobs/%s/builds/%d/build.xml" % (jta_home, testname, number)
build_file = open(build_xml, 'rb')
build_raw_values = build_file.read()
build_file.close()
build_dict = xmltodict.parse(build_raw_values)
start_sec = int(build_dict['build']['startTime']) / 1000
cur_dict['report']['starttime'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_sec))
log = "%s/jta/jobs/%s/builds/%d/log" % (jta_home, testname, number)
cur_dict['report']['endtime'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(os.path.getmtime(log)))
cur_dict['report']['result'] = str(build_dict['build']['result'])

# Get result and value of the testcase
cur_dict['report']['items'] = collections.OrderedDict({'item': []})
log_file = open(log, 'r')
log_raw_lines = log_file.readlines()
log_file.close()

for line in log_raw_lines:
	if line.startswith("++++ BOARD_VER"):
		cur_dict['report']['board_version'] = line.split("=")[1].strip()
	if line.startswith("++++ DEVICE_TEST"):
		cur_dict['report']['device'] = line.split("=")[1].strip()
	if line.startswith("++++ DEVICE_FS"):
		cur_dict['report']['filesystem'] = line.split("=")[1].strip()
	if line.startswith("++++ TESTLOG"):
		test_log = line.split("=")[1].strip()
	if line.startswith("+++ report "):
		tmp_cmd = line.split(";")[0]
		cur_dict['report']['test_dir'] = tmp_cmd.split()[3]
		tmp_cmd = line.split(";")[1].strip()
		cur_dict['report']['command_line'] = tmp_cmd.split("'")[0]
		break

cur_search_pat = re.compile("^(TEST-[\d]+) (.*)$", re.MULTILINE)
cur_file = open(test_log, 'r')
pat_result = cur_search_pat.findall(cur_file.read())
cur_file.close()

if pat_result:
	for i in range(0, len(pat_result)):
		ditem = collections.OrderedDict()
		cur_dict['report']['items']['item'].append(ditem)
		ditem['name'] = "%s" % pat_result[i][0]
		if ((pat_result[i][1]).strip() == "OK"):
			ditem['result'] = "PASS"
		else:
			ditem['result'] = "FAIL"
			ret_val = 1
if (ret_val == 1):
	cur_dict['report']['result'] = "FAILURE"
print cur_dict

content = xmltodict.unparse(cur_dict, pretty=True)

cur_xml = "%s/jta/jobs/%s/builds/%d/test_result.xml" % (jta_home, testname, number)
cur_file = open(cur_xml, 'wb')
cur_file.write(content)
cur_file.close()

sys.exit(ret_val)
