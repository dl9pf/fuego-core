tarball=commonAPI_SomeIp.tar.gz

function test_build {
    $CXX -std=gnu++11 HelloWorldService.cpp HelloWorldStubImpl.cpp HelloWorldSomeIPStubAdapter.cpp HelloWorldStubDefault.cpp HelloWorldSomeIPDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-SomeIP -lvsomeip -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -o HelloWorldService
    $CXX -std=gnu++11 HelloWorldClient.cpp HelloWorldSomeIPProxy.cpp HelloWorldSomeIPDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-SomeIP -lvsomeip -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -o HelloWorldClient   
}

function test_deploy {
	put HelloWorldService $BOARD_TESTDIR/fuego.$TESTDIR/
	put HelloWorldClient $BOARD_TESTDIR/fuego.$TESTDIR/
	put ./fidl/vsomeip.json $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; export VSOMEIP_CONFIGURATION_FILE=./fidl/vsomeip.json; chmod 777 *;
	if (VSOMEIP_APPLICATION_NAME=service-sample ./HelloWorldService &); then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
	if (VSOMEIP_APPLICATION_NAME=client-sample ./HelloWorldClient); then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;
        pkill HelloWorldServi"
}

function test_processing {
	log_compare "$TESTDIR" "2" "^TEST.*OK" "p"
    	log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh


