#!/bin/sh
# Script that automates the execution of LTP
# Usage (run from the top LTP dir):
# TESTS="syscalls fs"; PTSTESTS="MEM"; RTTESTS="rt-migrate"; .  ltp_target_run.sh

OUTPUT_DIR=${PWD}/result
TMP_DIR=${PWD}/tmp

[ -d ${TMP_DIR} ] && rm -rf ${TMP_DIR}
mkdir -p ${TMP_DIR}

[ -d ${OUTPUT_DIR} ] && rm -rf ${OUTPUT_DIR}
mkdir -p ${OUTPUT_DIR}

echo y | ./IDcheck.sh

echo "ltp_target_run: ${TESTS} | ${PTSTESTS} | ${RTTESTS}"

for i in ${TESTS}; do
    echo "ltp_target_run: doing test $i"
    mkdir -p ${OUTPUT_DIR}/${i}
    ./runltp -C ${OUTPUT_DIR}/${i}/failed.log \
             -l ${OUTPUT_DIR}/${i}/result.log \
             -o ${OUTPUT_DIR}/${i}/output.log \
             -d ${TMP_DIR} \
             -f $i > ${OUTPUT_DIR}/${i}/head.log 2>&1
    rm -rf ${TMP_DIR}/*
done

for i in ${PTSTESTS}; do
    echo "ltp_target_run: doing test $i"
    # PTS scripts seem to have some bashims
    /bin/bash ./bin/run-posix-option-group-test.sh $i
done
# run-posix-option-group-test.sh has been patched to generate pts.log
[ -e pts.log ] && cp pts.log ${OUTPUT_DIR}/

for i in ${RTTESTS}; do
    echo "ltp_target_run: doing test $i"
    /bin/bash ./testscripts/test_realtime.sh -t $i
done
[ -e testcases/realtime/logs ] && cat testcases/realtime/logs/*.log > ${OUTPUT_DIR}/rt.log

# LTP always success, check the results to see which subtests failed
exit 0
