{
    "testName": "Functional.LTP",
    "specs":
    [
        {
            "name": "default",
            "tests": "syscalls SEM"
        },
        {
            "name": "selection",
            "tests": "syscalls fs pipes sched timers dio mm ipc pty AIO MSG SEM SIG THR TMR TPS"
        },
        {
            "name": "selectionwithrt",
            "tests": "syscalls fs pipes sched timers dio mm ipc pty AIO MSG SEM SIG THR TMR TPS func/hrtimer-prio func/pi-tests func/rt-migrate func/sched_latency func/sched_jitter"
        },
        {
            "name": "ltplite",
            "tests": "ltplite"
        },
        {
            "name": "ptsonly",
            "tests": "AIO MEM MSG SEM SIG THR TMR TPS"
        },
        {
            "name": "smoketest",
            "tests": "fs_bind"
        },
        {
            "name": "rtonly",
            "tests": "func/hrtimer-prio func/pi-tests func/rt-migrate func/sched_latency func/sched_jitter"
        }
    ]
}
