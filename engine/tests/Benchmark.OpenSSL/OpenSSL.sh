# some functions are shared between Benchmark.OpenSSL and Functional.OpenSSL
tarball=../OpenSSL/openssl-0.9.8j.tar.gz
source $FUEGO_CORE/engine/tests/OpenSSL/openssl.sh

function test_deploy {
    put apps $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; apps/openssl speed"
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
