#!/bin/bash

tarball=neon-test.tar.bz2

function test_build {
	chmod -R 777 *
	patch -p0 < gnutls_4.3_fixup.patch
	patch -p0 < pkgconfig.patch
	cd ./neon-0.30.1
	libtoolize --automake
	aclocal --system-acdir=${SDKROOT}usr/share/aclocal
	autoreconf --verbose --install --force --exclude=autopoint -I macros
	autoconf
	autoheader
	automake -a
	./configure --host=$PREFIX --with-libxml2 -with-expat --enable-shared --without-gssapi --with-ssl=gnutls CPPFLAGS="-DHAVE_GNUTLS_CERTIFICATE_SET_RETRIEVE_FUNCTION=1"
	make
	cd ./test
	make
	chmod -R 777 *
	./makekeys

	gzip -c --no-name ../NEWS > file1.gz
	gzip -c --name ../NEWS > file2.gz
	gzip -c --no-name ../NEWS > trailing.gz
	echo "hello, world" >> trailing.gz
	dd of=badcsum.gz if=file1.gz bs=1 count=`perl -e 'printf "%d", (stat("file1.gz"))[7] - 8;'`

	echo 'broken!' >> badcsum.gz
	dd if=file1.gz of=truncated.gz bs=2048 count=2

	dd of=corrupt1.gz if=file1.gz bs=1 count=500

	cat ../NEWS >> corrupt1.gz
	cat ../NEWS > corrupt2.gz
	touch empty.gz
	cat ../NEWS > random.txt
	echo foobar > foobar.txt
}

function test_deploy {
	put -r ./neon-0.30.1/test/* $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod -R 777 *; \
	if ./acl3744; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
	if ./auth; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
	if ./basic; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi; \
	if ./compress; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi; \
	if ./lock; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi; \
	if ./oldacl; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi; \
	if ./props; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi; \
	if ./redirect; then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi; \
	if ./request; then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi; \
	if ./session; then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi; \
	if ./socket; then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi; \
	if ./socket-ssl; then echo 'TEST-12 OK'; else echo 'TEST-12 FAIL'; fi; \

	if ./string-tests; then echo 'TEST-14 OK'; else echo 'TEST-14 FAIL'; fi; \
	if ./stubs; then echo 'TEST-15 OK'; else echo 'TEST-15 FAIL'; fi; \
	if ./uri-tests; then echo 'TEST-16 OK'; else echo 'TEST-16 FAIL'; fi; \
	if ./util-tests; then echo 'TEST-17 OK'; else echo 'TEST-17 FAIL'; fi; \
	if ./xml; then echo 'TEST-18 OK'; else echo 'TEST-18 FAIL'; fi; \
	if ./xmlreq; then echo 'TEST-19 OK'; else echo 'TEST-19 FAIL'; fi"
#	if ./ssl; then echo 'TEST-13 FAIL'; else echo 'TEST-13 OK'; fi; \
}

function test_processing {
	P_CRIT="TEST.*"
	N_CRIT="TEST.*FAIL"

	log_compare "$TESTDIR" "18" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh


