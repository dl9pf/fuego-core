#!/bin/bash

tarball=libpcap-tests.tar.gz

function test_build {
	$CC -fpic  -O2 -pipe -g -feliminate-unused-debug-types -DHAVE_CONFIG_H  -D_U_="__attribute__((unused))" -I. -o capturetest capturetest.c -lpcap	
	$CC -fpic  -O2 -pipe -g -feliminate-unused-debug-types -DHAVE_CONFIG_H  -D_U_="__attribute__((unused))" -I. -o filtertest filtertest.c -lpcap	 
	$CC -fpic  -O2 -pipe -g -feliminate-unused-debug-types -DHAVE_CONFIG_H  -D_U_="__attribute__((unused))" -I. -o findalldevstest findalldevstest.c -lpcap	
	$CC -fpic  -O2 -pipe -g -feliminate-unused-debug-types -DHAVE_CONFIG_H  -D_U_="__attribute__((unused))" -I. -o opentest opentest.c -lpcap	
	$CC -fpic  -O2 -pipe -g -feliminate-unused-debug-types -DHAVE_CONFIG_H  -D_U_="__attribute__((unused))" -I. -o reactivatetest reactivatetest.c -lpcap	
	$CC -fpic  -O2 -pipe -g -feliminate-unused-debug-types -DHAVE_CONFIG_H  -D_U_="__attribute__((unused))" -I. -o selpolltest selpolltest.c -lpcap	
	$CC -fpic  -O2 -pipe -g -feliminate-unused-debug-types -DHAVE_CONFIG_H  -D_U_="__attribute__((unused))" -I. -o valgrindtest valgrindtest.c -lpcap	
}

function test_deploy {
	put capturetest filtertest findalldevstest opentest reactivatetest selpolltest valgrindtest $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *; \
	if capturetest -c 10 ; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
	if filtertest 1 ; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
	if findalldevstest ; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi; \
	if opentest ; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi; \
	if reactivatetest ; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi; \
	if selpolltest -c 10 ; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi; \
	if valgrindtest ; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi"
}

function test_processing {
	P_CRIT="TEST.*OK"
	N_CRIT="TEST.*FAIL"

	log_compare "$TESTDIR" "7" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh



