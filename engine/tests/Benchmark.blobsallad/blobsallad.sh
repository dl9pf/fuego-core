tarball=blobsallad-src.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/blobsallad.Makefile.patch
    patch -p0 -N -s < $TEST_HOME/blobsallad.auto.patch
    patch -p0 -N -s < $TEST_HOME/bs_main.c.patch

    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" SDKROOT="$SDKROOT"  && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put blobsallad  $BOARD_TESTDIR/fuego.$TESTDIR/
	put maps  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export DISPLAY=:0; xrandr | awk '/\*/ {split(\$1, a, \"x\"); exit(system(\"./blobsallad \" a[1]  a[2]))}'"
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
