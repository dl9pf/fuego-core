#!/bin/python
# See common.py for description of command-line arguments

import os, re, sys
sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

ref_section_pat = "^\[[\w\d_ .]+.[gle]{2}\]"

# Total time: 36.03
cur_search_pat = re.compile("^Total time: (\d+.\d+)",re.MULTILINE)
#cur_search_pat = re.compile("^(.*)(Gtk)([\w -]{0,21})(\ -\ time:\ {1,6})(\d+.\d+)",re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
print pat_result
if pat_result:
	cur_dict['Total_time'] = pat_result[0]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'Time.ms'))
