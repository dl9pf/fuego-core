tarball=gtkperf-0.40.tar.bz2

function test_build {
	cd src
	patch -p0 -N -s < $TEST_HOME/gtkperf_callbacks.c.patch
	patch -p0 -N -s < $TEST_HOME/gtkperf_interface.c.patch
	patch -p0 -N -s < $TEST_HOME/gtkperf_main.c.patch
	patch -p0 -N -s < $TEST_HOME/gtkperf_appdata.h.patch
	cd ..
	export PATH=/usr/local/bin:$PATH
        ./configure --prefix=$BOARD_TESTDIR/$TESTDIR --host=$HOST --build=`uname -m`-linux-gnu --target=$HOST
        make
    # || { cd src; $CC -o .libs/gtkperf support.o main.o interface.o callbacks.o tests.o timing.o  -L${SDKROOT}/usr/lib ${SDKROOT}/usr/lib/libgtk-x11-2.0.so ${SDKROOT}/usr/lib/libatk-1.0.so ${SDKROOT}/usr/lib/libgio-2.0.so ${SDKROOT}/usr/lib/libpangoft2-1.0.so ${SDKROOT}/usr/lib/libfreetype.so ${SDKROOT}/usr/lib/libfontconfig.so ${SDKROOT}/usr/lib/libgdk-x11-2.0.so ${SDKROOT}/usr/lib/libgdk_pixbuf-2.0.so ${SDKROOT}/usr/lib/libpangocairo-1.0.so ${SDKROOT}/usr/lib/libpango-1.0.so ${SDKROOT}/usr/lib/libcairo.so ${SDKROOT}/usr/lib/libgobject-2.0.so ${SDKROOT}/usr/lib/libgmodule-2.0.so ${SDKROOT}/usr/lib/libglib-2.0.so -Wl,--rpath -Wl,${SDKROOT}/usr/lib -Wl,--sysroot=${SDKROOT}; } && touch ../test_suite_ready || build_error "error while building test"
}

function test_deploy {
	cmd "gdk-pixbuf-query-loaders > /etc/gtk-2.0/gdk-pixbuf.loaders && pango-querymodules > /etc/pango/pango.modules && mkdir -p $BOARD_TESTDIR/fuego.$TESTDIR/share/pixmaps"
	put src/gtkperf  $BOARD_TESTDIR/fuego.$TESTDIR/
	put pixmaps/*.png  $BOARD_TESTDIR/fuego.$TESTDIR/share/pixmaps
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export DISPLAY=:0; xrandr | awk '/\*/ {split(\$1,a,\"x\"); exit(system(\"./gtkperf -a -x\" a[1]\" -y\" a[2]))}'"  
}

function test_cleanup {
	kill_procs gtkperf
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
