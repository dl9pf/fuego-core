function test_build {
   touch test_suite_ready
}

function test_deploy {
   true
}

function test_run {
report "if curl -o test.html www.baidu.com > /dev/null; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
if curl -i -r 0-1024 http://www.sina.com.cn -o sina_part1.html > /dev/null; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;\
if curl -v www.baidu.com > /dev/null; then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;\
if curl -V > /dev/null; then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi;\
if curl -y 30 www.baidu.com > /dev/null; then echo 'TEST-5 OK'; else echo 'TEST-5 FAILED'; fi;\
if curl -q www.baidu.com > /dev/null;then echo Enter && echo 'TEST-6 OK'; else echo 'TEST-6 FAILED'; fi"
}

function test_processing {
    log_compare "$TESTDIR" "6" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh
