tarball=linus_stress.tar.gz

function test_build {
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put linus_stress  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	assert_define FUNCTIONAL_LINUS_STRESS_DIRTY_BG
	assert_define FUNCTIONAL_LINUS_STRESS_DIRTY

	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        cat /proc/sys/vm/dirty_ratio > ./dirty_ratio; \
        cat /proc/sys/vm/dirty_background_ratio > ./dirty_bg; \
        echo $FUNCTIONAL_LINUS_STRESS_DIRTY > /proc/sys/vm/dirty_ratio || echo 'WARNING: /proc/sys/vm/dirty_ratio is read-only'; \
        echo $FUNCTIONAL_LINUS_STRESS_DIRTY_BG > /proc/sys/vm/dirty_background_ratio || echo 'WARNING: /proc/sys/vm/dirty_background_ratio is read-only'; \
        ./linus_stress; \
        cat ./dirty_ratio > /proc/sys/vm/dirty_ratio; \
        cat ./dirty_bg > /proc/sys/vm/dirty_background_ratio || echo 'WARNING: /proc/sys/vm/dirty_background_ratio is read-only'"
}

function test_processing {
	true
}

. $FUEGO_CORE/engine/scripts/functional.sh
