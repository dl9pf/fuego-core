#!/bin/sh
# scan_for_items.sh - look for specific items on the target
# support for the following is assumed (and mandatory)
# echo, shell functions, command line redirection

BOARD_TESTDIR=$1
if test -z "$BOARD_TESTDIR" ; then
    echo "Usage scan_for_items.sh \<board_testdir\>"
    exit 1
fi

set +e

RESULT="SUCCESS"
echo "== Fuego target feature scan results =="

check_for_program () {
    command -v "$1" >/dev/null 2>&1
}

# you would think that the following could be done in a loop
# but without a sure-fire way to convert to upper-case, no.

if check_for_program cat ; then
    echo HAS_PROGRAM_CAT=1
else
    echo HAS_PROGRAM_CAT=0
    RESULT="FAILURE - Missing 'cat'"
fi

if check_for_program logread ; then
    echo HAS_PROGRAM_LOGREAD=1	
    HAS_PROGRAM_LOGREAD=1
else
    echo HAS_PROGRAM_LOGREAD=0
    HAS_PROGRAM_LOGREAD=0
fi

if check_for_program logger ; then
    echo HAS_PROGRAM_LOGGER=1
    HAS_PROGRAM_LOGGER=1
else
    echo HAS_PROGRAM_LOGGER=0
    HAS_PROGRAM_LOGGER=1
fi

if check_for_program touch ; then
    echo HAS_PROGRAM_TOUCH=1
else
    echo HAS_PROGRAM_TOUCH=0
    RESULT="FAILURE - Missing 'touch'"
fi

if check_for_program tee ; then
    echo HAS_PROGRAM_TEE=1
else
    echo HAS_PROGRAM_TEE=0
    RESULT="FAILURE - Missing 'tee'"
fi

if check_for_program rm ; then
    echo HAS_PROGRAM_RM=1
else
    echo HAS_PROGRAM_RM=0
    RESULT="FAILURE - Missing 'rm'"
fi

if check_for_program true ; then
    echo HAS_PROGRAM_TRUE=1
else
    echo HAS_PROGRAM_TRUE=0
    RESULT="FAILURE - Missing 'true'"
fi

if check_for_program "[" ; then
    echo HAS_PROGRAM_BRACKET=1
else
    echo HAS_PROGRAM_BRACKET=0
    RESULT="FAILURE - Missing '['"
fi

if check_for_program "mount" ; then
    echo HAS_PROGRAM_MOUNT=1
else
    echo HAS_PROGRAM_MOUNT=0
    RESULT="FAILURE - Missing 'mount'"
fi

if check_for_program "umount" ; then
    echo HAS_PROGRAM_UMOUNT=1
else
    echo HAS_PROGRAM_UMOUNT=0
    RESULT="FAILURE - Missing 'umount'"
fi

if check_for_program "find" ; then
    echo HAS_PROGRAM_FIND=1
else
    echo HAS_PROGRAM_FIND=0
    RESULT="FAILURE - Missing 'find'"
fi

if check_for_program "head" ; then
    echo HAS_PROGRAM_HEAD=1
else
    echo HAS_PROGRAM_HEAD=0
    RESULT="FAILURE - Missing 'head'"
fi

if check_for_program "mkdir" ; then
    echo HAS_PROGRAM_MKDIR=1
else
    echo HAS_PROGRAM_MKDIR=0
    RESULT="FAILURE - Missing 'mkdir'"
fi

if check_for_program "rmdir" ; then
    echo HAS_PROGRAM_RMDIR=1
else
    echo HAS_PROGRAM_RMDIR=0
    RESULT="FAILURE - Missing 'rmdir'"
fi

# used by ov_firmware
if check_for_program "uname" ; then
    echo HAS_PROGRAM_UNAME=1
else
    echo HAS_PROGRAM_UNAME=0
    RESULT="FAILURE - Missing 'uname'"
fi

# used by ov_firmware - we should get rid of this usage
if check_for_program "xargs" ; then
    echo HAS_PROGRAM_XARGS=1
else
    echo HAS_PROGRAM_XARGS=0
    RESULT="FAILURE - Missing 'xargs'"
fi

# used by ov_rootfs_reboot
if check_for_program "reboot" ; then
    echo HAS_PROGRAM_REBOOT=1
else
    echo HAS_PROGRAM_REBOOT=0
    RESULT="FAILURE - Missing 'reboot'"
fi

# used by ov_rootfs_state
if check_for_program "uptime" ; then
    echo HAS_PROGRAM_UPTIME=1
else
    echo HAS_PROGRAM_UPTIME=0
    RESULT="FAILURE - Missing 'uptime'"
fi

# used by ov_rootfs_state
if check_for_program "free" ; then
    echo HAS_PROGRAM_FREE=1
else
    echo HAS_PROGRAM_FREE=0
    RESULT="FAILURE - Missing 'free'"
fi

# used by ov_rootfs_state
if check_for_program "df" ; then
    echo HAS_PROGRAM_DF=1
else
    echo HAS_PROGRAM_DF=0
    RESULT="FAILURE - Missing 'df'"
fi

# used by ov_rootfs_state
if check_for_program "ps" ; then
    echo HAS_PROGRAM_PS=1
else
    echo HAS_PROGRAM_PS=0
    RESULT="FAILURE - Missing 'ps'"
fi

# used by ov_rootfs_state
if check_for_program "grep" ; then
    echo HAS_PROGRAM_GREP=1
else
    echo HAS_PROGRAM_GREP=0
    RESULT="FAILURE - Missing 'grep'"
fi

# used by ov_rootfs_sync
if check_for_program "sync" ; then
    echo HAS_PROGRAM_SYNC=1
else
    echo HAS_PROGRAM_SYNC=0
    RESULT="FAILURE - Missing 'sync'"
fi

# used by ov_rootfs_kill - we should get rid of this usage
# can simulate with ps | grep and regular kill
if check_for_program "pkill" ; then
    echo HAS_PROGRAM_PKILL=1
else
    echo HAS_PROGRAM_PKILL=0
    RESULT="FAILURE - Missing 'pkill'"
fi

# test for specific arguments:
# tee -a - assumed to be an intrinsic feature of 'tee'
# true vs. /bin/true?
# head -n - assumed to be an intrinsic feature of 'head'
# find -name - assumed to be an intrinsic feature of 'find'
# pkill -9 - assumed to be an intrinsic feature of 'pkill'

# mkdir -p
mkdir -p foo/bar
if test -d foo/bar ; then
   echo HAS_MKDIR_P=1
else
   echo HAS_MKDIR_P=0
   RESULT="FAILURE - mkdir missing -p"
fi
rmdir foo/bar
rmdir foo

# rm -rf
mkdir foo-local-fuego-test
touch foo-local-fuego-test/bar
rm -rf foo-local-fuego-test
if test ! -e foo-local-fuego-test/bar ; then
   echo HAS_RM_RF=1
else
   echo HAS_RM_RF=0
   RESULT="FAILURE - rm missing -rf"
fi
rm foo-local-fuego-test/bar 2>/dev/null
rmdir foo-local-fuego-test 2>/dev/null

# grep -Fv - used by ov_rootfs_state - we should get rid of -F
cat <<eof1 >local-fuego-test-grep
1  foo
2  [foo]
eof1

LINE_COUNT=$(cat local-fuego-test-grep | grep -Fv "  [" | wc -l)
if test $LINE_COUNT -eq "1" ; then
   echo HAS_GREP_F=1
else
   echo HAS_GREP_F=0
   RESULT="FAILURE - grep missing -F"
fi
rm local-fuego-test-grep

# now check for specific binaries and files
# /sbin/reboot, /sbin/true, /sbin/logread
# /var/log/messages, /var/log/syslog
# /proc/interrupts, /proc/sys/vm/drop_caches, /proc/$$/oom_score_adj

if test -x /sbin/reboot ; then
   echo HAS_SBIN_REBOOT=1
else
   echo HAS_SBIN_REBOOT=0
   RESULT="FAILURE - missing /sbin/reboot"
fi

if test -x /bin/true ; then
   echo HAS_BIN_TRUE=1
else
   echo HAS_BIN_TRUE=0
   RESULT="FAILURE - missing /bin/true"
fi

if test -x /sbin/route ; then
   echo HAS_SBIN_ROUTE=1
else
   echo HAS_SBIN_ROUTE=0
   RESULT="FAILURE - missing /sbin/route"
fi

if test -e /var/log/message ; then
   echo HAS_VAR_LOG_MESSAGES=1
   HAS_VAR_LOG_MESSAGES=1
else
   echo HAS_VAR_LOG_MESSAGES=0
   HAS_VAR_LOG_MESSAGES=0
fi

if test -e /var/log/syslog ; then
   echo HAS_VAR_LOG_SYSLOG=1
   HAS_VAR_LOG_SYSLOG=1
else
   echo HAS_VAR_LOG_SYSLOG=0
   HAS_VAR_LOG_SYSLOG=0
fi

if test -e /proc/interrupts ; then
   echo HAS_PROC_INTERRUPTS=1
else
   echo HAS_PROC_INTERRUPTS=0
   RESULT="FAILURE - missing /proc/interrupts"
fi

if test -e /proc/sys/vm/drop_caches ; then
   echo HAS_PROC_DROP_CACHES=1
else
   echo HAS_PROC_DROP_CACHES=0
fi

if test -e /proc/\$\$/oom_score_adj ; then
   echo HAS_PROC_OOM_SCORE_ADJ=1
else
   echo HAS_PROC_OOM_SCORE_ADJ=0
fi

if test -e /proc/config.gz ; then
   echo HAS_PROC_CONFIG_GZ=1
else
   echo HAS_PROC_CONFIG_GZ=0
fi

##########################################
# now get some purely information stuff
#HAS_MEMORY
MEMORY_LINE=$(free | grep Mem:)
set -- junk $MEMORY_LINE
HAS_MEMORY=$3
echo "HAS_MEMORY=$HAS_MEMORY K"

STORAGE_LINE=$(df $BOARD_TESTDIR | tail -n 1)
set -- junk $STORAGE_LINE
HAS_STORAGE=$3
echo "HAS_STORAGE=$HAS_STORAGE K"

#########################################
# make a distribution recommendation
if test "$HAS_PROGRAM_LOGGER" = "1" ; then
    if test "$HAS_PROGRAM_LOGREAD" = "1" ; then
        RECOMMENDED_DISTRIB="distribs/base.dist"
    else
        RECOMMENDED_DISTRIB="distribs/nologread.dist"
    fi
else
    RECOMMENDED_DISTRIB="distribs/nosyslogd.dist"
fi

if test "$HAS_VAR_LOG_MESSAGES" = "0" ; then
    RECOMMENDED_DISTRIB="distribs/nosyslogd.dist"
fi

echo "-------------------------------------"
echo "Based on this scan,"
echo "the recommended DISTRIB value should be:"
echo "   $RECOMMENDED_DISTRIB"
echo "-------------------------------------"

echo "RESULT=$RESULT"
if test "$RESULT" = "SUCCESS" ; then
    echo "All required programs, files, features, and permissions are present"
else
    echo "One or more required items are missing" 
fi
echo "-------------------------------------"

