#!/bin/bash

tarfile=scan_for_items.sh

function test_build {
    touch test_suite_ready
}

function test_deploy {
    put $TEST_HOME/scan_for_items.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./scan_for_items.sh $BOARD_TESTDIR"

    # now do some host-driven tests

    # do a transport speed test
    tmpfile=$(mktemp)
    filename=$(basename $tmpfile)
    dd if=/dev/urandom bs=1000 count=10 of=$tmpfile
    put_start=$(date +"%s.%N")
    put $tmpfile $BOARD_TESTDIR/fuego.$TESTDIR/
    put_end=$(date +"%s.%N")
    put_time=$(python -c "print \"%.2f\" % ($put_end - $put_start)")

    put_str="Time to put 10K file: $put_time"
    rm "$tmpfile"

    get_start=$(date +"%s.%N")
    get $BOARD_TESTDIR/fuego.$TESTDIR/$filename $tmpfile
    get_end=$(date +"%s.%N")
    get_time=$(python -c "print \"%.2f\" % ($get_end - $get_start)")
    get_str="Time to get 10K file: $get_time"

    rm "$tmpfile"
    cmd "rm $BOARD_TESTDIR/fuego.$TESTDIR/$filename"

    cmd_start=$(date +"%s.%N")
    for i in $(seq 10) ; do
        cmd true ;
    done
    cmd_end=$(date +"%s.%N")
    cmd_time=$(python -c "print \"%.2f\" % ($cmd_end - $cmd_start)")
    cmd_str="Time to do 10 cmds: $cmd_time"
    cmd1_time=$(python -c "print \"%.2f\" % ($cmd_time / 10 )")
    cmd1_str="Average time per cmd: $cmd1_time"

    report_append "echo Data from host:"
    report_append "echo TRANSPORT=$TRANSPORT"
    report_append "echo $put_str"
    report_append "echo $get_str"
    report_append "echo $cmd_str"
    report_append "echo $cmd1_str"

}

function test_processing {
    log_compare "$TESTDIR" "1" "SUCCESS" "p"
}

. $FUEGO_CORE/engine/scripts/functional.sh
