tarball=NetPIPE-3.7.1.tar.gz

function test_build {
    patch -p1 -N -s < ../../tarballs/netpipe-makefile.patch
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put NPtcp  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	cmd "killall -SIGKILL NPtcp 2>/dev/null; exit 0"

	# Start netpipe server on Jenkins host
	netpipe_exec=`which NPtcp`

	if [ -z $netpipe_exec ];
	then 
	 echo "ERROR: Cannot find netpipe"
	 false
	else
	 $netpipe_exec -p 2 &
	fi

        assert_define BENCHMARK_NETPIPE_PERT

	if [ "$BENCHMARK_NETPIPE_SRV" = "default" ]; then
	  srv=$SRV_IP
	else
	  srv=$BENCHMARK_NETPIPE_SRV
	fi

	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./NPtcp -h $srv -p $BENCHMARK_NETPIPE_PERT" $BOARD_TESTDIR/fuego.$TESTDIR/${TESTDIR}.log
}

source $FUEGO_CORE/engine/scripts/functions.sh

source $FUEGO_CORE/engine/scripts/overlays.sh
set_overlay_vars


pre_test $TESTDIR

if $Rebuild; then
    build
fi

deploy

test_run


