{
    "testName": "Functional.synctest",
    "specs": 
    [
        {
            "name":"sata",
            "MOUNT_BLOCKDEV":"$SATA_DEV",
            "MOUNT_POINT":"$SATA_MP",
            "LEN":"10",
            "LOOP":"10"
        },
        {
            "name":"mmc",
            "MOUNT_BLOCKDEV":"$MMC_DEV",
            "MOUNT_POINT":"$MMC_MP",
            "LEN":"10",
            "LOOP":"10"
        },
        {
            "name":"usb",
            "MOUNT_BLOCKDEV":"$USB_DEV",
            "MOUNT_POINT":"$USB_MP",
            "LEN":"10",
            "LOOP":"10"
        },
        {
            "name":"default",
            "MOUNT_BLOCKDEV":"ROOT",
            "MOUNT_POINT":"$BOARD_TESTDIR/work",
            "LEN":"10",
            "LOOP":"10"
        }
    ]
}

                

                      
