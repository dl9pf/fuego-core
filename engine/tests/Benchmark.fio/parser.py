#!/bin/python
# See common.py for description of command-line arguments

import os, re, sys
sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

ref_section_pat = "^\[[\w\d_ .]+.[gle]{2}\]"
cur_search_pat = re.compile("(  READ:|  WRITE:)(.*)(aggrb=)([\d.]+)(KB\/s)",re.MULTILINE)

res_dict = {}
cur_dict = {}

pat_result = plib.parse(cur_search_pat)
if pat_result:
	cur_dict["SSD_Read.Seq"] = '%.2f' % float(pat_result[0][3])
	cur_dict["SSD_Read.Random"] = '%.2f' % float(pat_result[1][3])
	cur_dict["SSD_Write.Seq"] = '%.2f' % float(pat_result[2][3])
	cur_dict["SSD_Write.Random"] = '%.2f' % float(pat_result[3][3])
	cur_dict["File.Read"] = '%.2f' % float(pat_result[4][3])
	cur_dict["File.Write"] = '%.2f' % float(pat_result[5][3])

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'Avg bandwidth.KB/s'))
