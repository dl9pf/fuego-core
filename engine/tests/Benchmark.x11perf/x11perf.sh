tarball=x11perf-1.5.4.tar.gz

function test_build {
    sed -i '/XORG_MACROS_VERSION/d' ./configure.ac
    sed -i '/xorg-macros 1.8/d' ./configure.ac

    ./autogen.sh --host $HOST

    ./configure $CONFIGURE_FLAGS X11PERF_LIBS="-lX11 -lXmu" # force to use libXmu instead of libXmuu
    sed -i 's#$(SED)#sed#' Makefile # this is wrong, but somewhy $(SED) in Makefile expands to nothing
    make && touch test_suite_ready
}

function test_deploy {
	put x11perf  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_X11PERF_TIME
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export DISPLAY=:0; ./x11perf -repeat 1 -time $BENCHMARK_X11PERF_TIME -dot -oddtilerect10 -seg100c2 -64poly10complex"  
}

. $FUEGO_CORE/engine/scripts/benchmark.sh

