tarball=bonnie++-1.03e.tar.gz

function test_build {
    ./configure --host=$HOST --build=`uname -m`-linux-gnu;
    make && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put bonnie++  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_BONNIE_MOUNT_BLOCKDEV
    assert_define BENCHMARK_BONNIE_MOUNT_POINT
    assert_define BENCHMARK_BONNIE_SIZE
    assert_define BENCHMARK_BONNIE_RAM
    assert_define BENCHMARK_BONNIE_ROOT

    hd_test_mount_prepare $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT

    if [ "$BENCHMARK_BONNIE_ROOT" == "true" ]; then
        report "cd $BOARD_TESTDIR/fuego.$TESTDIR; pwd; ls; ./bonnie\+\+ -d $BENCHMARK_BONNIE_MOUNT_POINT/fuego.$TESTDIR -u 0:0 -s $BENCHMARK_BONNIE_SIZE -r $BENCHMARK_BONNIE_RAM"
    else
        report "cd $BOARD_TESTDIR/fuego.$TESTDIR; pwd; ls; ./bonnie\+\+ -d $BENCHMARK_BONNIE_MOUNT_POINT/fuego.$TESTDIR -s $BENCHMARK_BONNIE_SIZE -r $BENCHMARK_BONNIE_RAM"
    fi

    sleep 60

    hd_test_clean_umount $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
