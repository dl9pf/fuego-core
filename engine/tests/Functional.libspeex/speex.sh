tarball=speex.tar.gz

function test_build {
    ./configure $CONFIGURE_FLAGS
    make
}

function test_deploy {
    put src/.libs/speexdec $BOARD_TESTDIR/fuego.$TESTDIR/
    put src/.libs/speexenc $BOARD_TESTDIR/fuego.$TESTDIR/
    put 1.wav $BOARD_TESTDIR/fuego.$TESTDIR/    
}

function test_run {
report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *;
if ./speexenc 1.wav 1.speex; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
if ./speexdec 1.speex 2.wav;then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi"
}

function test_processing {
    log_compare "$TESTDIR" "2" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh
