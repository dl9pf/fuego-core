tarball=glibc_tests.tar.gz

function test_build {
    CC="$CC" AR="$AR" RANLIB="$RANLIB"
    make all

    echo "#!/bin/bash
    # ./c is test case for libc and ld
    if ./c;              then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi;
    if ./pthread 10;     then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi;
    if ./rt;             then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi;
    if ./util;           then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi;
    if ./resolv mit.edu; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi;
    if ./m;              then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi;
    if ./crypt;          then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi;
    if ./dl;             then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi;
    if ./anl localhost;  then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi;
    if ./nsl;            then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi;

    useradd test;
    passwd test << EOF
hello
hello
EOF
    if ./nss_compat test hello;  then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi;
    userdel test; rm /home/test/ -rf;

    cp /etc/nsswitch.conf nsswitch.conf.bk;
    sed 's/^hosts.*$/hosts:          files/g' nsswitch.conf.bk > /etc/nsswitch.conf;
    if ./nss localhost;   then echo 'TEST-12 OK'; else echo 'TEST-12 FAIL'; fi;
    cp nsswitch.conf.bk /etc/nsswitch.conf;

    cp /etc/nsswitch.conf nsswitch.conf.bk;
    sed 's/^hosts.*$/hosts:          dns/g' nsswitch.conf.bk > /etc/nsswitch.conf;
    if ./nss localhost;   then echo 'TEST-13 OK'; else echo 'TEST-13 FAIL'; fi;
    cp nsswitch.conf.bk /etc/nsswitch.conf;

    if [ -f /lib/libBrokenLocale-2.23.so ];
                         then echo 'TEST-14 OK'; else echo 'TEST-14 FAIL'; fi;
    " > run-tests.sh

    touch test_suite_ready
}

function test_deploy {
    put run-tests.sh test_bin/* $BOARD_TESTDIR/fuego.$TESTDIR/;
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v run-tests.sh"  
}

function test_processing {
    log_compare "$TESTDIR" "14" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh
