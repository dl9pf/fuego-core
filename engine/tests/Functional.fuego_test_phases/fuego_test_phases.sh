#!/bin/bash

tarball=none

export PHASE_STR="script load"

# use string concatenation to test phase ordering
function test_pre_check {
    PHASE_STR="${PHASE_STR}:test_pre_check"
    # if you don't use nohup, ssh transport will hang trying to background fuegosleep
    is_on_target nohup PROGRAM_NOHUP /usr/bin
    assert_define PROGRAM_NOHUP
}

function test_build {
    PHASE_STR="${PHASE_STR}:test_build"
    touch test_suite_ready
}

function test_deploy {
    PHASE_STR="${PHASE_STR}:test_deploy"
    true
}

function test_run {
    PHASE_STR="${PHASE_STR}:test_run"
    # leave a process running on the target
    cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR; ln -sf /bin/sleep fuegosleep ; $PROGRAM_NOHUP ./fuegosleep 10000 >/dev/null 2>&1 &"
    report 'echo "Started fuegosleep command from test_run" ; echo "PHASE_STR (so far)=${PHASE_STR}"'
}

function test_processing {
    PHASE_STR="${PHASE_STR}:test_processing"
    true
}

function test_cleanup {
    PHASE_STR="${PHASE_STR}:test_cleanup"

    set +e

    # last chance to put something into the log
    # but have to do it directly
    LOGFILE=${LOGDIR}/testlog.txt
    echo "########" >>$LOGFILE
    echo "Items from here on are from 'test_cleanup' (after the actual test run)" >>$LOGFILE

    # see if fuegosleep is still running on target
    TMPFILE="/tmp/$$-${RANDOM}"
    TMPFILE2="/tmp/$$-${RANDOM}"
    cmd "ps | grep fuegosleep | grep -v grep >$TMPFILE"
    get $TMPFILE $TMPFILE2
    cmd "rm $TMPFILE"

    echo "'fuegosleep' should appear in this log exactly once" >>$LOGFILE
    echo "before kill_procs:" >>$LOGFILE
    cat $TMPFILE2 >>$LOGFILE
    rm $TMPFILE2

    # try to kill fuegosleep
    kill_procs fuegosleep

    # check if fuegosleep is still running on target
    TMPFILE="/tmp/$$-${RANDOM}"
    TMPFILE2="/tmp/$$-${RANDOM}"
    cmd "ps | grep fuegosleep | grep -v grep >$TMPFILE"
    get $TMPFILE $TMPFILE2
    cmd "rm $TMPFILE"
    echo "after kill_procs:" >>$LOGFILE
    cat $TMPFILE2 >>$LOGFILE
    rm $TMPFILE2

    echo "PHASE_STR=$PHASE_STR" >>$LOGFILE
    set -e
}

. $FUEGO_CORE/engine/scripts/functional.sh
