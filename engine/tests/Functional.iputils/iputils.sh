#tarball=testtmp.tar.gz

function test_build {
	echo   "testtmp.sh build"
}

function test_deploy {	
	echo   "testtmp.sh deploy"
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *;
	(ifconfig eth0 inet6 add 2001:da8:2004:1000:202:116:160:41/61);
	(route -A inet6 add default gw 2001:da8:2004:1000:1);
	if traceroute6 ipv6.scau.edu.cn; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
	if clockdiff -o localhost ; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;\
	if ping -c 4 www.baidu.com; then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;\
	if tracepath www.baidu.com; then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi;\
	if ping6 -c 5 ::1; then echo 'TEST-5 OK'; else echo 'TEST-5 FAILED'; fi;\
	if tracepath6 ipv6.scau.edu.cn; then echo 'TEST-6 OK'; else echo 'TEST-6 FAILED'; fi"
}

function test_processing {
	log_compare "$TESTDIR" "6" "^TEST.*OK" "p"
    	log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh



