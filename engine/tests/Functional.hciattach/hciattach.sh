function test_build {
    echo True
}

function test_deploy {
    echo True
}

function test_run {
report "if hciattach -l;then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi"
}

function test_processing {
    log_compare "$TESTDIR" "1" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh
