tarball=java_perf.tar

function test_build {
    touch test_suite_ready
}

function test_deploy {
    put *.jar  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; java -cp scimark2lib.jar jnt.scimark2.commandline"  
    report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar avrora" $BOARD_TESTDIR/ 
    report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar jython"  
    report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar luindex"  
    report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar lusearch"  
    #report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar tomcat"  
    report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar xalan"  
}

function test_cleanup {
    # this seems dangerous - what if there are other java programs running?
    #kill_procs java
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
