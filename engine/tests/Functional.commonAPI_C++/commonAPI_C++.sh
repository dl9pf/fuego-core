tarball=commonAPI_C++.tar.gz

function test_build {
    $CXX -std=gnu++11 ./commonAPI_Dbus/HelloWorldService.cpp ./commonAPI_Dbus/HelloWorldStubImpl.cpp ./commonAPI_Dbus/HelloWorldDBusStubAdapter.cpp ./commonAPI_Dbus/HelloWorldStubDefault.cpp ./commonAPI_Dbus/HelloWorldDBusDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-DBus -ldbus-1 -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -I $SDKTARGETSYSROOT/usr/include/dbus-1.0/ -I $SDKTARGETSYSROOT/usr/lib/dbus-1.0/include/ -o HelloWorldService_Dbus
    $CXX -std=gnu++11 ./commonAPI_Dbus/HelloWorldClient.cpp ./commonAPI_Dbus/HelloWorldDBusProxy.cpp ./commonAPI_Dbus/HelloWorldDBusDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-DBus -ldbus-1 -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -I $SDKTARGETSYSROOT/usr/include/dbus-1.0/ -I $SDKTARGETSYSROOT/usr/lib/dbus-1.0/include/ -o HelloWorldClient_Dbus

    $CXX -std=gnu++11 ./commonAPI_SomeIp/HelloWorldService.cpp ./commonAPI_SomeIp/HelloWorldStubImpl.cpp ./commonAPI_SomeIp/HelloWorldSomeIPStubAdapter.cpp ./commonAPI_SomeIp/HelloWorldStubDefault.cpp ./commonAPI_SomeIp/HelloWorldSomeIPDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-SomeIP -lvsomeip -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -o HelloWorldService_SomeIp
    $CXX -std=gnu++11 ./commonAPI_SomeIp/HelloWorldClient.cpp ./commonAPI_SomeIp/HelloWorldSomeIPProxy.cpp ./commonAPI_SomeIp/HelloWorldSomeIPDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-SomeIP -lvsomeip -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -o HelloWorldClient_SomeIp
}

function test_deploy {
	put HelloWorldService_Dbus $BOARD_TESTDIR/fuego.$TESTDIR/
	put HelloWorldClient_Dbus $BOARD_TESTDIR/fuego.$TESTDIR/

	put HelloWorldService_SomeIp $BOARD_TESTDIR/fuego.$TESTDIR/
	put HelloWorldClient_SomeIp $BOARD_TESTDIR/fuego.$TESTDIR/
	put ./commonAPI_SomeIp/fidl/vsomeip.json $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	

	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; export VSOMEIP_CONFIGURATION_FILE=./fidl/vsomeip.json; chmod 777 *;
	if (./HelloWorldService_Dbus &); then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
	if ./HelloWorldClient_Dbus; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;
	pkill HelloWorldServi

	if (VSOMEIP_APPLICATION_NAME=service-sample ./HelloWorldService_SomeIp &); then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;\
	if (VSOMEIP_APPLICATION_NAME=client-sample ./HelloWorldClient_SomeIp); then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi;
    pkill HelloWorldServi"
}

function test_processing {
	log_compare "$TESTDIR" "4" "^TEST.*OK" "p"
    	log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh


