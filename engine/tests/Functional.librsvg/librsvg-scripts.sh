#!/bin/bash

tarball=librsvg-2.40.15.tar.bz2

function test_build {
	libtoolize --automake
	aclocal --system-acdir=${SDKROOT}/usr/share/aclocal
	autoconf
	autoheader
	automake -a
	./configure --host=$PREFIX --enable-always-build-tests --disable-pixbuf-loader
	make rsvg-convert
	cd tests
	make
	./rsvg-test
	./styles
	./crash
	cd ../tools
	make
}

function test_deploy {
	put ./.libs/rsvg-convert  $BOARD_TESTDIR/fuego.$TESTDIR/
	put ./tests/.libs/*  $BOARD_TESTDIR/fuego.$TESTDIR/
	put -r ./tests/fixtures  $BOARD_TESTDIR/fuego.$TESTDIR/
	put ./tools/.libs/*  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod -R 777 *; \
	if rsvg-convert -f png -d 100 -p 100 -x 2 -y 2 -z 1 -w 100 -h 100 -a -b white ./fixtures/reftests/duplicate-id.svg > duplicate-id_test.png; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
	if rsvg-convert -f pdf ./fixtures/reftests/duplicate-id.svg > duplicate-id.pdf; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
	if rsvg-convert -f ps ./fixtures/reftests/duplicate-id.svg > duplicate-id.ps; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi; \
	if rsvg-convert -f eps ./fixtures/reftests/duplicate-id.svg > duplicate-id.eps; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi; \
	if rsvg-convert -f svg ./fixtures/reftests/duplicate-id.svg > duplicate-id_test.svg; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi; \
	if rsvg-convert -o test ./fixtures/reftests/duplicate-id.svg; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi; \ 
	if rsvg-dimensions ./fixtures/reftests/duplicate-id.svg; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi; \ 
	if test-performance ./fixtures/reftests/duplicate-id.svg; then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi; \ 
	if lt-rsvg-test; then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi; \ 
	if lt-styles; then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi; \ 
	if lt-crash; then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi"  
}

function test_processing {
	P_CRIT="TEST.*OK"
	N_CRIT="TEST.*FAIL"

	log_compare "$TESTDIR" "11" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh


