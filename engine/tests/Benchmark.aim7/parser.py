#!/bin/python
# See common.py for description of command-line arguments

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

ref_section_pat = "\[[\w]+.[gle]{2}\]"
cur_search_pat = re.compile("^Max Jobs per Minute\s*([\d]+\.[\d]+)",re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)

print pat_result
if pat_result:
	cur_dict["short"] = pat_result[0]
	cur_dict["utime"] = pat_result[1]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'FPS'))
