#!/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

ref_section_pat = "^\[[\w\d._]+.[gle]{2}\]"
cur_test_pat = re.compile("([\w]+)(\s+)([\d.]+)(\s...\s)([\d.]+)(\s+)([\d.]+)(.+)")

cur_dict = {}
cur_file = open(plib.TEST_LOG,'r')
print "Reading current values from " + plib.TEST_LOG +"\n"

lines = cur_file.readlines()
cur_file.close()

groups = [ "Audio","Video", "X", "Gaming" ]
tests = [ "None", "Video", "X", "Burn", "Write", "Read", "Compile", "Memload" ]

start_pos = {}
for linenum, line in enumerate(lines):
	if "Benchmarking simulated cpu of" in line:
		l = linenum + 2
		if "Audio" in line: start_pos["Audio"] = l
   		elif "Video" in line: start_pos["Video"] = l
		elif "X" in line: start_pos["X"] = l
		elif "Gaming" in line: start_pos["Gaming"] = l

# print "lines: %s" % (lines)
# print "start_pos: %s" % (start_pos)

for group in groups:
	enum_tests = enumerate(tests)
	index = start_pos.get(group)
        print "group: %s, index: %s, lines[index]: %s" % (group, index, lines[index])
	for i, v in enum_tests:
		if v != group:
                        mt = cur_test_pat.match(lines[index])
                        if mt == None:
                                print "Warning: Can't parse %s.%s" % (group, v)
                                continue
                        else:
                                cur_dict[group+'.'+v] = '%.2f' % float(mt.group(7))
			index += 1

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'Max latency, ms'))
