function test_build {
    touch test_suite_ready
}

function test_deploy {
    return 0
}

function test_run {
    RESULT=$(( 1000 + (RANDOM % 600 - 300) ))
    report "echo fuego_check_plots result: $RESULT"
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
