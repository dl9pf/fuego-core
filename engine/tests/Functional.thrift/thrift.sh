tarball=thrift-0.9.3.tar.gz

function test_build {
    echo "#!/bin/bash
    tar zxf git.tar.gz
    cd git/tutorial
    thrift -r --gen py tutorial.thrift
    thrift -r --gen cpp tutorial.thrift
    ls
    echo \"#### build-test.sh\"
    tar zcf gen-cpp.tar.gz gen-cpp
    " > build-tests.sh

    echo "#!/bin/bash
    cd git/tutorial
    if [ -e gen-py/__init__.py ] ; then
    echo 'TEST-1 OK'
    else
    echo 'TEST-1 FAILED'
    fi

    if [ -e gen-py/tutorial/__init__.py ] ; then
    echo 'TEST-2 OK'
    else
    echo 'TEST-2 FAILED'
    fi

    if [ -e gen-py/tutorial/ttypes.py ] ; then
    echo 'TEST-3 OK'
    else
    echo 'TEST-3 FAILED'
    fi

    if [ -e gen-py/tutorial/Calculator.py ] ; then
    echo 'TEST-4 OK'
    else
    echo 'TEST-4 FAILED'
    fi

    if [ -e gen-py/tutorial/constants.py ] ; then
    echo 'TEST-5 OK'
    else
    echo 'TEST-5 FAILED'
    fi

    if [ -e gen-py/tutorial/Calculator-remote ] ; then
    echo 'TEST-6 OK'
    else
    echo 'TEST-6 FAILED'
    fi

    if [ -e gen-py/shared ] ; then
    echo 'TEST-7 OK'
    else
    echo 'TEST-7 FAILED'
    fi

    if [ -e gen-py/shared/__init__.py ] ; then
    echo 'TEST-8 OK'
    else
    echo 'TEST-8 FAILED'
    fi

    if [ -e gen-py/shared/ttypes.py ] ; then
    echo 'TEST-9 OK'
    else
    echo 'TEST-9 FAILED'
    fi

    if [ -e gen-py/shared/SharedService.py ] ; then
    echo 'TEST-10 OK'
    else
    echo 'TEST-10 FAILED'
    fi

    if [ -e gen-py/shared/constants.py ] ; then
    echo 'TEST-11 OK'
    else
    echo 'TEST-11 FAILED'
    fi

    if [ -e gen-py/shared/SharedService-remote ] ; then
    echo 'TEST-12 OK'
    else
    echo 'TEST-12 FAILED'
    fi
    
    cp py/PythonClient.py  py/PythonServer.py gen-py/
    cd gen-py
    ./PythonServer.py & 
    sleep 1
    ./PythonClient.py

    if [ \$? -eq 0 ] ; then
    echo 'TEST-13 OK'
    else
    echo 'TEST-13 FAILED'
    fi

    killall python

    cd $BOARD_TESTDIR/fuego.$TESTDIR/; \
    ./CppServer &
    sleep 1
    ./CppClient
    if [ \$? -eq 0 ] ; then
    echo 'TEST-14 OK'
    else
    echo 'TEST-14 FAILED'
    fi

    killall CppServer
    " > run-tests.sh
    touch test_suite_ready
}

function test_deploy {
    tar zcf git.tar.gz git
    put -r git.tar.gz build-tests.sh run-tests.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR \
    tar zxf git.tar.gz;
    sh -v build-tests.sh 2>&1"
    cd $WORKSPACE/$TRIPLET
    pwd
    ls
    get $BOARD_TESTDIR/fuego.$TESTDIR/git/tutorial/gen-cpp.tar.gz .; pwd
    tar zxf gen-cpp.tar.gz -C git/tutorial/;
    cd git/tutorial/cpp
    CPPFILES="../gen-cpp/shared_types.cpp ../gen-cpp/tutorial_types.cpp ../gen-cpp/tutorial_constants.cpp ../gen-cpp/Calculator.cpp ../gen-cpp/SharedService.cpp"
    ${CXX} CppServer.cpp ${CPPFILES} -o CppServer -I../../lib/cpp/src/ -lthrift
    ${CXX} CppClient.cpp ${CPPFILES} -o CppClient -I../../lib/cpp/src/ -lthrift
    put CppServer CppClient $BOARD_TESTDIR/fuego.$TESTDIR/
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
    sh -v run-tests.sh 2>&1"
}

function test_processing {
    log_compare "$TESTDIR" "14" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh

