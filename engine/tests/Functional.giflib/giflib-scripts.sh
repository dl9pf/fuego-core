#!/bin/bash

tarball=giflib-4.1.6.tar.bz2

function test_build {
	./configure $CONFIGURE_FLAGS
	make
}

function test_deploy {
	put pic/* util/.libs/* $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *; echo 11111111 > test_raw.raw; \
	if gifbg -d tl -s 320 200 -c 255 255 255 -l 64 > bg1.gif; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
	if gifwedge > test_gifwedge.gif; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
	if gif2epsn cover.gif > cover.epsn; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi; \
	if gif2ps cover.gif > cover.ps; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi; \
	if gif2rgb -o cover cover.gif; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi; \
	if gifasm bg1.gif porsche.gif > test_gifasm.gif; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi; \
	if gifclip -i 222 0 390 134 solid2.gif > test_gifclip.gif; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi; \
	if gifclrmp -i 2 cover.gif > test_gifclrmp.gif; then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi; \
	if gifcomb porsche.gif bg1.gif > test_gifcomb.gif; then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi; \
	if text2gif -t 'abcde' > abcde.gif; then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi; \
	if giffiltr <abcde.gif > test_giffiltr.gif; then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi; \
	if gifspnge <porsche.gif > test_gifspnge.gif; then echo 'TEST-12 OK'; else echo 'TEST-12 FAIL'; fi; \
	if giffix cover.gif > test_giffix.gif; then echo 'TEST-13 OK'; else echo 'TEST-13 FAIL'; fi; \
	if gifflip -r solid2.gif > test_gifflip.gif; then echo 'TEST-14 OK'; else echo 'TEST-14 FAIL'; fi; \
	if gifhisto -b -s 200 512 porsche.gif > test_gifhisto.gif; then echo 'TEST-15 OK'; else echo 'TEST-15 FAIL'; fi; \
	if gifinfo  cover.gif; then echo 'TEST-16 OK'; else echo 'TEST-16 FAIL'; fi; \
	if gifinter cover.gif > test_gifinter.gif; then echo 'TEST-17 OK'; else echo 'TEST-17 FAIL'; fi; \
	if gifbg | gifinto test_gifinto.gif; then echo 'TEST-18 OK'; else echo 'TEST-18 FAIL'; fi; \
	if gifbg | gifovly > test_gifovly.gif; then echo 'TEST-19 OK'; else echo 'TEST-19 FAIL'; fi; \
	if gifpos -s 720 348 -i 400 148 porsche.gif > test_gifpos.gif; then echo 'TEST-20 OK'; else echo 'TEST-20 FAIL'; fi; \
	if gifrotat -a 45 cover.gif > test_gifrotat.gif; then echo 'TEST-21 OK'; else echo 'TEST-21 FAIL'; fi; \
	if gifrsize -s 0.45 solid2.gif > test_gifrsize.gif; then echo 'TEST-22 OK'; else echo 'TEST-22 FAIL'; fi; \
	if giftext cover.gif > giftext.txt; then echo 'TEST-23 OK'; else echo 'TEST-23 FAIL'; fi; \
	if icon2gif sample.ico > test_icon2gif.gif; then echo 'TEST-24 OK'; else echo 'TEST-24 FAIL'; fi; \
	if raw2gif -s 1 1 test_raw.raw > test_raw2gif.gif; then echo 'TEST-25 OK'; else echo 'TEST-25 FAIL'; fi; \
	if gif2rgb x-trans.gif | rgb2gif -s 100 100  > test_rgb2gif.gif; then echo 'TEST-26 OK'; else echo 'TEST-26 FAIL'; fi"
}

function test_processing {
	P_CRIT="TEST.*OK"
	N_CRIT="TEST.*FAIL"

	log_compare "$TESTDIR" "26" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}

. $FUEGO_CORE/engine/scripts/functional.sh



