#!/bin/bash

tarball=fuego-transport-1.0.tgz

function test_build {
	return 0
}

function test_deploy {
	return 0
}

function test_run {
    # copy some files to the target under different conditions
    # get is tested by the system, with log retrieval
    desc="test single file put"
    DESTDIR=$BOARD_TESTDIR/fuego.$TESTDIR
    put bartop $DESTDIR
    report "if [ -f $DESTDIR/bartop ] ; then echo \"ok 1 $desc\" ; else echo \"not ok 1 $desc\" ; fi" 
    cmd "rm $DESTDIR/bartop"

    desc="test multiple file put"
    put footop foo2top $DESTDIR
    report_append "if [ -f $DESTDIR/footop -a -f $DESTDIR/foo2top ] ; then echo \"ok 2 $desc\" ; else echo \"not ok 2 $desc\" ; fi" 
    cmd "rm $DESTDIR/foo*top"

    desc="test wildcard file put"
    put foo*top $DESTDIR
    report_append "if [ -f $DESTDIR/footop -a -f $DESTDIR/foo2top ] ; then echo \"ok 3 $desc\" ; else echo \"not ok 3 $desc\" ; fi" 
    cmd "rm $DESTDIR/foo*top"

    desc="test recursive directory put"
    put dir1 $DESTDIR
    report_append "if [ -f $DESTDIR/dir1/d1foo -a -f $DESTDIR/dir1/d1bar ] ; then echo \"ok 4 $desc\" ; else echo \"not ok 4 $desc\" ; fi" 
    cmd "rm $DESTDIR/dir1/*"
    cmd "rmdir $DESTDIR/dir1"

    desc="test multiple recursive dir put"
    put dir1 dir2 $DESTDIR
    report_append "if [ -f $DESTDIR/dir1/d1foo -a -f $DESTDIR/dir1/d1bar -a -f $DESTDIR/dir2/d2foo -a -f $DESTDIR/dir2/d2bar ] ; then echo \"ok 5 $desc\" ; else echo \"not ok 5 $desc\" ; fi" 
    cmd "rm $DESTDIR/dir1/* $DESTDIR/dir2/*"
    cmd "rmdir $DESTDIR/dir1 $DESTDIR/dir2"

    desc="test multiple files, wildcards and recursive dirs put"
    put bartop foo*top dir* $DESTDIR
    report_append "if [ -f $DESTDIR/bartop -a -f $DESTDIR/footop -a -f $DESTDIR/foo2top -a -f $DESTDIR/dir1/d1foo -a -f $DESTDIR/dir1/d1bar -a -f $DESTDIR/dir2/d2foo -a -f $DESTDIR/dir2/d2bar ] ; then echo \"ok 6 $desc\" ; else echo \"not ok 6 $desc\" ; fi" 
    cmd "rm $DESTDIR/bartop $DESTDIR/foo*top $DESTDIR/dir1/* $DESTDIR/dir2/*"
    cmd "rmdir $DESTDIR/dir1 $DESTDIR/dir2"

    # FIXTHIS - should try some file encodings that would be problematic for some transports
}

function test_processing {
    log_compare "$TESTDIR" "6" "^ok" "p"
}

. $FUEGO_CORE/engine/scripts/functional.sh
