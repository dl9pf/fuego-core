# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains a sequence of calls that are needed for running functional test

if [ -n "$FUEGO_DEBUG" ] ; then
	set -x
fi
set -e

source $FUEGO_CORE/engine/scripts/overlays.sh
set_overlay_vars

source $FUEGO_CORE/engine/scripts/reports.sh
source $FUEGO_CORE/engine/scripts/functions.sh

echo "##### doing fuego phase: pre_test ########"
pre_test $TESTDIR

echo "##### doing fuego phase: build ########"
if $Rebuild; then
    build
fi

echo "##### doing fuego phase: deploy ########"
deploy

echo "##### doing fuego phase: run ########"
test_run

echo "##### doing fuego phase: get_testlog ########"
get_testlog $TESTDIR

echo "##### doing fuego phase: processing ########"
FUEGO_RESULT=0
set +e
call_if_present test_processing
export FUEGO_RESULT=$?
set -e

post_test $TESTDIR
echo "Fuego: all test phases complete!"
return $FUEGO_RESULT
