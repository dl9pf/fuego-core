#!/usr/bin/python

# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import sys, os, re
import simplejson as json

FUEGO_CORE=os.environ['FUEGO_CORE']
FUEGO_RW=os.environ['FUEGO_RW']
TESTDIR=os.environ['TESTDIR']

RESULT_PATH = '%s/logs/%s/' % (FUEGO_RW, TESTDIR)
DATAFILE    = '%s/logs/%s/plot.data' % (FUEGO_RW, TESTDIR)
REF_DATA    = '%s/engine/tests/%s/reference.log' % (FUEGO_CORE, TESTDIR)

with open(REF_DATA) as f:
    ref_raw_data = f.readlines()

with open(DATAFILE) as f:
    data = f.readlines()

# reflog format: [section|criteria], section can be 'groupname.test' or 'test'
# Example: [sha512.8192bytes|ge]
reflog_regex_pattern = "^\[([\w\d&._/()-]+)\|([gle]{2})\]"

groups = {}
for item in ref_raw_data:
    try:
        item = item[:item.index('#')].strip()  # strip comments
    except ValueError: # no comment found
        pass
    m = re.match(reflog_regex_pattern, item)
    if m:
        section  = m.groups()[0]
        criteria = m.groups()[1]

        if '.' in section:
            groupname, test = section.split('.')
        else:
            groupname, test = section, section

        for line in data:
            line_split = line.split()
            ITEM_NODE_NAME = line_split[0]
            ITEM_TESTSPEC  = line_split[2]

            label = '%s-%s-%s.%s' % (ITEM_NODE_NAME, ITEM_TESTSPEC, groupname, test)
            info  = [{ 'data' : [], 'points' : { 'symbol': "circle" }, 'label' : label },
                     { 'data' : [], 'points' : { 'symbol': "cross" }, 'label' : label + '.ref' }]
            if not groups.get(groupname):
                groups[groupname] = info
            else:
                # append info if the same label does not exist already
                label_found = False
                for group in groups[groupname]:
                    if label == group.get('label'):
                        label_found = True
                        break
                if label_found == False:
                    groups[groupname] += info

# Read log file with approx. strucure:
# NODE_NAME TESTDIR TESTSPEC BUILD_NUMBER BUILD_ID BUILD_TIMESTAMP FWVER PLATFORM SECTION REF.VALUE CUR.VALUE

info = {}
for line in data:
    line_split = line.split()
    ITEM_NODE_NAME       = line_split[0]
    ITEM_TESTDIR         = line_split[1]
    ITEM_TESTSPEC        = line_split[2]
    ITEM_BUILD_NUMBER    = line_split[3]
    ITEM_BUILD_ID        = line_split[4]
    ITEM_BUILD_TIMESTAMP = line_split[5]
    ITEM_FWVER           = line_split[6]
    ITEM_PLATFORM        = line_split[7]
    ITEM_SECTION         = line_split[8]
    ITEM_REF_VALUE       = line_split[9]
    ITEM_CUR_VALUE       = line_split[10]

    if '.' in ITEM_SECTION:
        groupname, test = ITEM_SECTION.split('.')
    else:
        groupname, test = ITEM_SECTION, ITEM_SECTION

    for item in groups.get(groupname):
        label = '%s-%s-%s.%s' % (ITEM_NODE_NAME, ITEM_TESTSPEC, groupname, test)
        if '.ref' in item['label']:
            item_label = item['label'].replace('.ref','')
            if (item_label == label):
                item['data'].append((ITEM_BUILD_NUMBER, float(ITEM_REF_VALUE)))
        elif (label in item['label']):
                item['data'].append((ITEM_BUILD_NUMBER, float(ITEM_CUR_VALUE)))

        if info.has_key(ITEM_BUILD_NUMBER) == False:
                info[ITEM_BUILD_NUMBER] = [ITEM_BUILD_NUMBER, ITEM_FWVER, ITEM_PLATFORM, ITEM_NODE_NAME]

#print "info: " + str(info)
#info: {'1': ['1', '4.4.0-70-generic', 'fake', 'docker'], '3': ['3', '4.4.0-70-generic', 'fake', 'docker'], '2': ['2', '4.4.0-70-generic', 'fake', 'docker']}

devices = []
for m in sorted(info.keys()):
    string = info.get(m)

    bid = string[0]
    fw = string[1]
    sdk = string[2]
    dev = string[3]

    if len(devices) == 0:
        devices.append({'device':dev,'info':[[bid],[fw],[sdk]]})

    flag = False
    for i in devices:
        if dev == i.get('device'):
            flag = True
            d = i

    if flag:
        d.get('info')[0].append(bid)
        d.get('info')[1].append(fw)
        d.get('info')[2].append(sdk)

    if flag == False:
        devices.append({'device':dev,'info':[[bid],[fw],[sdk]]})

devices.sort()

# write metric data file
for metric in sorted(set(groups)):
    RESULT = RESULT_PATH+TESTDIR+'.'+metric+'.json'
    print "Writing results to ", RESULT
    rf = open(RESULT,'w')
    rf.write(json.dumps(groups.get(metric),sort_keys=True))
    rf.close

INF_FILE = RESULT_PATH+TESTDIR+'.info.json'
print "Writing info file:", INF_FILE
inf = open(INF_FILE,'w')
inf.write(json.dumps(devices,sort_keys=True))
inf.close

# copy metrics info file
# this has a list of metrics for this test, in json format
TEST_METRICS_SRC = '%s/engine/tests/%s/metrics.json' % (FUEGO_CORE, TESTDIR)
METRICS_FILE = RESULT_PATH + 'metrics.json'
try:
    data = open(TEST_METRICS_SRC,'r').read()
except:
    print "Warning: missing %s (flot will use tests.info instead)" % TEST_METRICS_SRC
    data = ""

if data:
    print "Writing metrics file:", METRICS_FILE
    mf = open(METRICS_FILE,'w')
    mf.write(data)
    mf.close

def main():
    pass

if __name__ == '__main__':
    main()
