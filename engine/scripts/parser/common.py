#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Cogent Embedded, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
common.py - This library contains parsing functions
Created by Konstantin Belov on 2011-07-28.
Generalized by Dmitry Semyonov
"""

import sys, os, re, json

import matplotlib
matplotlib.use('Agg')
import pylab as plot

FUEGO_RW=os.environ['FUEGO_RW']
FUEGO_CORE=os.environ['FUEGO_CORE']
NODE_NAME=os.environ['NODE_NAME']
TESTDIR=os.environ['TESTDIR']
TESTSPEC=os.environ['TESTSPEC']
BUILD_NUMBER=os.environ['BUILD_NUMBER']
BUILD_ID=os.environ['BUILD_ID']
BUILD_TIMESTAMP=os.environ['BUILD_TIMESTAMP']
# FIXTHIS: why PLATFORM was not available!!?
PLATFORM='fake' #os.environ['PLATFORM']
FWVER=os.environ['FWVER']

PLOT_DATA = FUEGO_RW + '/logs/' + TESTDIR + '/plot.data'
PLOT_FILE = FUEGO_RW + '/logs/' + TESTDIR + '/plot.png'

REF_LOG  = '%s/engine/tests/%s/reference.log' % (FUEGO_CORE, TESTDIR)
TEST_LOG = '%s/logs/%s/%s.%s.%s.%s/testlog.txt' % (FUEGO_RW, TESTDIR, NODE_NAME, TESTSPEC, BUILD_NUMBER, BUILD_ID)

# FIXTHIS: the user should be able to change this variable
custom_write_report = False # the test will use custom methods to write report

def parse(cur_search_pat):
    print "Reading current values from " + TEST_LOG +"\n"
    cur_file = open(TEST_LOG, 'r')
    pat_result = cur_search_pat.findall(cur_file.read())
    cur_file.close()
    return pat_result

def write_report_results(rep_data):
        if 'GEN_TESTRES_FILE' in os.environ and (os.environ['GEN_TESTRES_FILE'] != ""):
                print "Using default report writing function\n"
                with open(os.environ['GEN_TESTRES_FILE'], 'w+') as f:
                        f.write(json.dumps(rep_data, indent=4))
                        print ("Wrote %s testres file" % (os.environ['GEN_TESTRES_FILE']))
        else:
                print ("Not writing testres file")

def process_data(ref_section_pat, test_results, plot_type, label):
    """
    Parameters
    ----------
    ref_section_pat: regular expression that matches the 'section' in a
      reference.log entry (FIXTHIS: use "^\[([\w\d&._/()-]+)\|([gle]{2})\]"
      for all of them
    test_results: dictionary where keys are reflog sections, and values
      are test results extracted from the test log.
    plot_type: single plot (s) multiplot (m, l, or xl)
    label: y-axis label (e.g.: MB/s)
    """
    if not test_results:
        sys.exit(1)

    if custom_write_report:
        write_report_results(cur_dict)

    thresholds, criteria = read_thresholds_data(ref_section_pat)
    rc = compare(thresholds, test_results, criteria)
    store_plot_data(thresholds, test_results)
    set_plot_properties(plot_type)

    # FIXTHIS: add other options for comparison etc
    if plot_type == 's':
        create_plot(label)
    else:
        create_multiplot(label)

    print "Saving plot file to " + PLOT_FILE
    plot.savefig(PLOT_FILE)

    return rc

def set_plot_properties(plot_type):
    """Defines plot parameters such as font size, figure size, etc to default values."""
    plot.rcParams['font.family'] = 'sans-serif'
    plot.rcParams['font.sans-serif'] = 'Helvetica'
    plot.rcParams['axes.titlesize'] = 14.0
    plot.rcParams['legend.fontsize'] = 10.0
    plot.rcParams['figure.dpi'] = 100
    plot.rcParams['figure.facecolor'] = 'w'
    plot.rcParams['figure.edgecolor'] = 'k'
    if (plot_type == "s"):
        plot.rcParams['font.size'] = 14.0
        plot.rcParams['figure.figsize'] = 19,6
    elif (plot_type == "m"):
        plot.rcParams['font.size'] = 14.0
        plot.rcParams['figure.figsize'] = 19,10
    elif (plot_type == "l"):
        plot.rcParams['font.size'] = 14.0
        plot.rcParams['figure.figsize'] = 19,12
    elif (plot_type == "xl"):
        plot.rcParams['font.size'] = 14.0
        plot.rcParams['figure.figsize'] = 19,35

def read_thresholds_data(section_pat):
    """Reads test names (defined by section_pat) and values from REF_LOG
       into reference result dictionary."""
    try:
        with open(REF_LOG) as ref_file:
            ref_raw_data = ref_file.readlines()
    except:
        hls("Log file " + REF_LOG + " not found.", "w")
        sys.exit(1)

    thresholds = {} # reference threshold values
    criteria = {} # decision criteria, ge (>=) or le (<=)
    for item in ref_raw_data:
        if '#' in item:
            item = item[:item.index('#')].strip()  # strip comments

        if (re.match(section_pat, item)):
            full_section = item.lstrip('[').rstrip(']\n')
            section, item_criteria = full_section.split('|') # as <section_name>|<criteria>
            thresholds[section] = ''
            criteria[section] = item_criteria

        # threshold value
        if (re.match("[0-9.]+", item)):
            thresholds[section] = item.rstrip('\n')

    if (len(thresholds) == 0):
        hls("Reference file " + REF_LOG + " is empty.", "e")
        sys.exit(1)

    if (set(thresholds) - set(criteria)):
        hls("ERROR: Results are missing in logfile.","e")
        sys.exit(1)

    return thresholds, criteria

def store_plot_data(thresholds, test_results):
    '''
    Stores data from dictionaries into plot.data.
    NODE_NAME TESTDIR TESTSPEC BUILD_NUMBER BUILD_ID BUILD_TIMESTAMP FWVER PLATFORM SECTION REF.VALUE CUR.VALUE
    Example: docker Benchmark.Dhrystone 100M 5 5 2017-03-31_04-52-42 4.4.0-70-generic x86_64 1 14285714.0
    '''
    if os.path.isfile(PLOT_DATA):
        plot_file = open(PLOT_DATA,"r+") # Read and write
        # make sure the data is not repeated
        data = plot_file.readlines()
        if len(data) > 0:
            if all([NODE_NAME == data[-1].split()[0], TESTDIR   == data[-1].split()[1],
                    TESTSPEC  == data[-1].split()[2], BUILD_NUMBER == data[-1].split()[3],
                    BUILD_ID  == data[-1].split()[4], BUILD_TIMESTAMP == data[-1].split()[5],
                    FWVER     == data[-1].split()[6], PLATFORM == data[-1].split()[7]]):
                print "\nWARNING: repeated parameters in %s: %s\n" % (PLOT_DATA, data[-1])
    else:
        plot_file = open(PLOT_DATA,"w") # Create new

    for key in sorted(test_results.iterkeys()):
        thresholds_split = thresholds[key].split()
        test_results_split = test_results[key].split()
        for i,u in enumerate(test_results_split):
            line = "%s %s %s %s %s %s %s %s %s %s %s\n" % (NODE_NAME, TESTDIR, TESTSPEC, BUILD_NUMBER, BUILD_ID, BUILD_TIMESTAMP, FWVER, PLATFORM, key, thresholds_split[i], test_results_split[i])
            plot_file.write(line)

    plot_file.close()
    print "Data file "+PLOT_DATA+" was updated."

# FIXTHIS: remove <date><bid><group.test><cur><ref>[<fw><sdk><dev>]
def create_plot(ylabel):
    """Generates plot from datafile. ylabel - label for Y-axis """

    if os.path.isfile(PLOT_DATA):
        print "Reading plot data from " + PLOT_DATA
        plot_file = open (PLOT_DATA,'r')
        plot_data = plot_file.readlines()
        thresholds_data = {}
        test_results_data = {}
        build_numbers = []
        sections = []

        for item in plot_data:
            data_split = item.split()
            ITEM_NODE_NAME       = data_split[0]
            ITEM_TESTDIR         = data_split[1]
            ITEM_TESTSPEC        = data_split[2]
            ITEM_BUILD_NUMBER    = data_split[3]
            ITEM_BUILD_ID        = data_split[4]
            ITEM_BUILD_TIMESTAMP = data_split[5]
            ITEM_FWVER           = data_split[6]
            ITEM_PLATFORM        = data_split[7]
            ITEM_SECTION         = data_split[8]
            ITEM_REF_VALUE       = data_split[9]
            ITEM_CUR_VALUE       = data_split[10]

            if any([ITEM_NODE_NAME != NODE_NAME,
                    ITEM_TESTDIR   != TESTDIR,
                    ITEM_TESTSPEC  != TESTSPEC]):
                continue

            if ITEM_BUILD_NUMBER not in build_numbers:
                build_numbers.append(ITEM_BUILD_NUMBER)

            if ITEM_SECTION not in sections:
                sections.append(ITEM_SECTION)

            if thresholds_data.get(ITEM_SECTION):
                thresholds_data[ITEM_SECTION] = thresholds_data[ITEM_SECTION] + ',' + ITEM_REF_VALUE
            else:
                thresholds_data[ITEM_SECTION] = ITEM_REF_VALUE

            if test_results_data.get(ITEM_SECTION):
                test_results_data[ITEM_SECTION] = test_results_data.get(ITEM_SECTION) + ',' + ITEM_CUR_VALUE
            else:
                test_results_data[ITEM_SECTION] = ITEM_CUR_VALUE

        # Figure configuration
        ax = plot.figure().add_subplot(111)
        ax.set_xlabel('Build number')
        ax.set_ylabel(ylabel)
        ax.set_title(TESTDIR)
        box = ax.get_position()
        ax.set_position([box.bounds[0], box.bounds[1], box.bounds[2] * 0.8, box.bounds[3]])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        # Hide these grid behind plot objects
        ax.set_axisbelow(True)
        bnum = int(BUILD_NUMBER)
        plotrange = build_numbers[-bnum:]

        for key in sorted(sections):
        # It is required to start not from index #0 but from #1
            ax.plot(plotrange, test_results_data.get(key).split(",")[-bnum:], 'o-', label='Test '+key)

        # Store min/max values for test data
        ymin, ymax = plot.ylim()

        for key in sorted(sections):
            ax.plot(plotrange, thresholds_data.get(key).split(',')[-bnum:], 'x--', label='Ref. '+key)

        # Set Y size based on test data, adding 7% to display properly set reference values
        plot.ylim(ymin * 0.93, ymax * 1.07)
        plot.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    else:
        hls("Plot data file not found","e")
        sys.exit(1)

def create_multiplot(ylabel):
    """
    Generates plot from datafile.
    ylabel - label for Y-axis
    """
    if os.path.isfile(PLOT_DATA):
        print "Reading plot data from " + PLOT_DATA
        plot_file = open (PLOT_DATA,'r')
        plot_data = plot_file.readlines()
        if (len(plot_data) == 0):
            hls("Plot data file "+PLOT_DATA+" is empty.","e")
            sys.exit(1)
        else:
            thresholds_data = {} # testname,REF.VALUE
            test_results_data = {} # testname,CUR.VALUE
            build_numbers = [] # build numbers
            sections = [] # test names

            for item in plot_data:
                data_split = item.split()
                ITEM_NODE_NAME       = data_split[0]
                ITEM_TESTDIR         = data_split[1]
                ITEM_TESTSPEC        = data_split[2]
                ITEM_BUILD_NUMBER    = data_split[3]
                ITEM_BUILD_ID        = data_split[4]
                ITEM_BUILD_TIMESTAMP = data_split[5]
                ITEM_FWVER           = data_split[6]
                ITEM_PLATFORM        = data_split[7]
                ITEM_SECTION         = data_split[8]
                ITEM_REF_VALUE       = data_split[9]
                ITEM_CUR_VALUE       = data_split[10]

                if any([ITEM_NODE_NAME != NODE_NAME,
                        ITEM_TESTDIR   != TESTDIR,
                        ITEM_TESTSPEC  != TESTSPEC]):
                    continue

                if ITEM_BUILD_NUMBER not in build_numbers:
                    build_numbers.append(ITEM_BUILD_NUMBER)

                if ITEM_SECTION not in sections:
                    sections.append(ITEM_SECTION)

                if thresholds_data.get(ITEM_SECTION):
                    thresholds_data[ITEM_SECTION] = thresholds_data[ITEM_SECTION] + ',' + ITEM_REF_VALUE
                else:
                    thresholds_data[ITEM_SECTION] = ITEM_REF_VALUE

                if test_results_data.get(ITEM_SECTION):
                    test_results_data[ITEM_SECTION] = test_results_data.get(ITEM_SECTION) + ',' + ITEM_CUR_VALUE
                else:
                    test_results_data[ITEM_SECTION] = ITEM_CUR_VALUE

            tests = {} # test in reference.log's section <groupname.test|ge>
            for section in sorted(test_results_data.iterkeys()):
                section_split = section.split(".")

                if (len(section_split) > 1):
                    if tests.get(section_split[0]):
                        tests[section_split[0]] = tests[section_split[0]] + ',' + '.'.join(section_split[1:])
                    else:
                        tests[section_split[0]] = ".".join(section_split[1:])

            pn = 0
            t_num = len(tests)
            if t_num > 4:
                h_space = 0.6
            else:
                h_space = 0.4

            #print "DEBUG:" + str(tests)
            for testgroup in tests:
                pn += 1
                tests_split = tests.get(testgroup).split(",")
                plot.figure(1).subplots_adjust(hspace = h_space, wspace = 0.5, left = 0.055, right  = 0.65)
                plot.figure(1).text(0.5, 0.95, TESTDIR,horizontalalignment='center',verticalalignment='bottom', fontsize=20)
                if (pn > 1):
                    ax = plot.figure(1).add_subplot(t_num,1,0+pn, sharex=ax)
                    # FIXTHIS: Need to create a list of test measument units.
                    #ax.set_ylabel(str(pn))
                else:
                    ax = plot.figure(1).add_subplot(t_num,1,0+pn, title=str(pn))
                ax.set_title(testgroup)
                if (pn == t_num):
                    ax.set_xlabel('Build number')

                ax.set_ylabel(ylabel)
                box = ax.get_position()
                ax.set_position([box.bounds[0], box.bounds[1], box.bounds[2], box.bounds[3]])
                # Put a legend to the right of the current axis
                ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
                ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
                # Hide these grid behind plot objects
                ax.set_axisbelow(True)
                bnum = int(BUILD_NUMBER)
                plotrange = build_numbers[-bnum:]

                for testname in tests_split:
                    test_num = len(tests_split)
                    skey = testgroup+'.'+testname

                    if len(plotrange) != len(thresholds_data.get(skey).split(",")[-bnum:]):
                        sys.exit("There is problem with data integrity in "+PLOT_DATA+" file.")
                    else:
                        ax.plot(plotrange, test_results_data.get(skey).split(",")[-bnum:], 'o-', label='Test '+testname)

                # Store min/max values for test data
                ymin, ymax = plot.ylim()

                for testname in tests_split:
                    test_num = len(tests_split)
                    skey = testgroup+'.'+testname
                    if len(plotrange) != len(thresholds_data.get(skey).split(",")[-bnum:]):
                        sys.exit("There is problems with data integrity in "+PLOT_DATA+" file.")
                    else:
                        ax.plot(plotrange, thresholds_data.get(skey).split(",")[-bnum:], 'x--', label='Ref. '+testname)

                # Set Y size based on test data, adding 7% to display properly set reference values
                plot.ylim(ymin * 0.93, ymax * 1.07)

                if test_num > 4:
                    lnc = 2
                else:
                    lnc = 1
                plot.legend(loc = 2,ncol = lnc, bbox_to_anchor=(1.02, 1.), borderaxespad=0.)
    else:
        hls('Plot data file ' + PLOT_DATA + 'not found','e')
        sys.exit(1)

def hls(string,type):

    if type == "w":
        type_string = "WARNING"
    else:
        type_string = "ERROR"

    print "########################### " + type_string + " ###############################"
    print string
    print "-----"

def compare(thresholds, test_results, criteria):
    """
    This function makes a decision about current results.
      Input: thresholds (from reference.log), test results dictionary, criteria dictionary.
      Return code: '1' is used for parsing errors, so use '2' instead for threshold errors
    """
    for key in test_results:
        thresholds_split = thresholds.get(key).split()
        test_results_split = test_results.get(key).split()

        for i, u in enumerate(test_results_split):
            comparison_result = cmp(float(test_results_split[i]), float(thresholds_split[i]))
            if criteria[key] == 'ge' and comparison_result < 0:
                hls("Test section %s: test result %s is smaller than threshold %s." % (key, test_results_split[i], thresholds_split[i]),'e')
                return 2
            elif criteria[key] == 'le' and comparison_result > 0:
                hls("Test section %s: test result %s is greater than threshold %s." % (key, test_results_split[i], thresholds_split[i]),'e')
                return 2
            else:
                print "Test section %s: test result %s satisfies (%s) threshold %s." % (key, test_results_split[i], criteria[key], thresholds_split[i])
    return 0

def main():
    pass

if __name__ == '__main__':
    main()

