# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains core functions of Fuego that needed for running tests

# These are supporting functions for test suites building process.
. $FUEGO_CORE/engine/scripts/common.sh

function signal_handler {
  # if we got here, something went wrong.  Let's clean up and leave
  echo "in signal_handler"
  post_test $TESTDIR
  exit 130
}

trap signal_handler SIGTERM SIGHUP SIGALRM SIGINT EXIT
# cause ERR trap setting to be visible inside functions
set -o errtrace

# Unpacks $tarball_path/$tarball into current directory.
# $1 - optional flag; if set to "nostrip",
#      the leading path components won't be stripped
function unpack {
  [ "$1" = "nostrip" ] && strip_opt= || strip_opt="--strip-components=1"

  case ${tarball/*./} in
    gz|tgz) key=z ;;
    bz2) key=j ;;
    tar) key= ;;
    *) echo "Unknown $tarball file format. Not unpacking."; return;;
  esac

  tar ${key}xf $TEST_HOME/$tarball $strip_opt
}

function is_empty {
# $1 - parameter

 if [ -z $1 ]; then
   echo "ERROR: EMPTY PARAMETER"
   exit
 fi
}

function report_devlog() {
    echo "$@" >> "${LOGDIR}/devlog.txt"
}

function get {
    report_devlog "get: $@"
    ov_transport_get "$@"
}

function put {
    report_devlog "put: $@"
    ov_transport_put "$@"
}

# These are supporting functions for target command running
# FIXTHIS: Add descriptions for parameters in every function
function cmd {
    report_devlog "cmd: $@"
    ov_transport_cmd "$@"
}

function safe_cmd {
# $1 - command to execute

  ov_rootfs_oom
  cmd "$@"
}

function report {
# $1 - remote shell command, $2 - test log file.
# $2 - optional, default: $TESTDIR/$TESTDIR.log

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}
  cmd "touch $RETCODE"

  if [ -z $2 ]; then
    echo "WARNING: test log file parameter empty, so will use default"
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR.log"
  else
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee $2"
  fi

  RESULT=$(cmd "cat $RETCODE")
  cmd "rm -f $RETCODE"
  return ${RESULT}
}

function report_append {
# $1 - remote shell command, $2 - test log file.

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}
  cmd "touch $RETCODE"

  if [ -z $2 ]; then
    echo "WARNING: test log file parameter empty, so will use default"
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee -a $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR.log"
  else
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee -a $2"
  fi

  RESULT=$(cmd "cat $RETCODE")
  cmd "rm -f $RETCODE"
  return ${RESULT}
}

function dump_syslogs {
# 1 - tmp dir, 2 - before/after

  is_empty $1
  is_empty $2

  ov_rootfs_logread "$@"
}

function concurrent_check {
  LOCKFILE="$WORKSPACE/$TRIPLET.build.lock"

  if [ -e ${LOCKFILE} ]; then

    while $(wget -qO- "$(cat ${LOCKFILE})/api/xml?xpath=*/building/text%28%29") && [ ! -e test_suite_ready ]
    do
      sleep 5
    done
  fi

  echo "${BUILD_URL}" > ${LOCKFILE}
}

function build_error () {
    touch build_failed
    abort_job "Build failed: $@"
}

# Wait for other builds of the same test running in parallel,
# process Rebuild flag, and unpack test sources if necessary.
# Returns 0 if actual build needs to be performed; 1 - otherwise.
# Build scripts must call this function in the beginning.
# $1 is passed directly to unpack().
function pre_build {
  cd ${WORKSPACE}
  upName=`echo "${JOB_NAME^^}"| tr '.' '_'`
  spec_tarball="${upName}_TARBALL_NAME"

  if [ ! -z "${!spec_tarball}" ]
  then
      echo "Using $spec_tarball=${!spec_tarball} from test spec"
      tarball=${!spec_tarball}
  fi

  mkdir -p $TRIPLET && cd $TRIPLET
  if concurrent_check; then
    [ "$Rebuild" = "true" ] && rm -rf *

    if [ -e build_failed ]; then
        rm $TRIPLET/build_failed
        rm -rf *
    fi

    if [ ! -e test_suite_ready ]; then
      unpack $1
      return 0
    fi
  fi
  return 1
}

function build {
  pre_build $1
  build_start_time=$(date +"%s.%N")
  test_build || return 1
  build_end_time=$(date +"%s.%N")
  build_duration=$(python -c "print $build_end_time - $build_start_time")
  echo "Fuego test_build duration=$build_duration seconds"
  post_build
}

function post_build {
  true
}

function deploy {
  echo "##### doing fuego phase: pre_deploy ########"
  pre_deploy
  echo "##### doing fuego phase: test_deploy ########"
  test_deploy
  echo "##### doing fuego phase: post_deploy ########"
  post_deploy
}

function pre_deploy {
  cd "$WORKSPACE"
  if [ ! -e "$TRIPLET" ]; then
    if build; then
      echo "Test builded(looks like first time)"
      cd $WORKSPACE/$TRIPLET
    else
      echo -e "Error reason: unable to change dir to $TRIPLET"
    fi
  elif [ -e "$TRIPLET/build_failed" ]; then
      echo "Seems the test failed to build last time. Rebuilding"

      if build; then
          cd $WORKSPACE/$TRIPLET
      fi
  else
    cd  $TRIPLET
  fi
}

function post_deploy {
  rm -f $LOCKFILE
}

function firmware {
  ov_get_firmware
  export FWVER="$FW"
}

function target_setup_route_to_host () {
    # $1 - subnet address
    # $2 - netmask
    # $3 - gateway address
    # $4 - network interface

    cmd "true" || abort_job "Cannot connect to $DEVICE via $TRANSPORT"
    cmd "if /sbin/route | grep $1; then echo \"route to $1 already configured\"; else /sbin/route add -net $1 netmask $2 gw $3 dev $4; fi"
}

function pre_test {
# $1 - testdir
# Make sure the target is alive, and prepare workspace for the test
  source $FUEGO_RO/toolchains/tools.sh
  export SSHPASS=$PASSWORD

  is_empty $1

  # Setup routing to target if needed
  [ -n "$TARGET_SETUP_LINK" ] && $TARGET_SETUP_LINK

# Target cleanup flag check
  [ "$Target_PreCleanup" = "true" ] && target_cleanup $1 || true

  cmd "true" || abort_job "Cannot connect to $DEVICE via $TRANSPORT"

  export LOGDIR="$FUEGO_RW/logs/$TESTDIR/${NODE_NAME}.${TESTSPEC}.${BUILD_NUMBER}.${BUILD_ID}"

# call test_pre_check if defined
  call_if_present test_pre_check

# Get target device firmware.
  firmware
  cmd "echo \"Firmware revision:\" $FWVER" || abort_job "Error while ROOTFS_FWVER command execution on target"

# XXX: Sync date/time between target device and framework host
# Also log memory and disk status as well as non-kernel processes,and interrupts

  ov_rootfs_state

  cmd "if [ ! -d $BOARD_TESTDIR ]; then mkdir -p $BOARD_TESTDIR; fi" || abort_job "ERROR: cannot find nor create $BOARD_TESTDIR"

  local fuego_test_dir=$BOARD_TESTDIR/fuego.$1

  # use a /tmp dir in case logs should be on a different partition
  # a board file can override the default of /tmp by setting FUEGO_TARGET_TMP
  local fuego_test_tmp=${FUEGO_TARGET_TMP:-/tmp}/fuego.$1

  cmd "rm -rf ${fuego_test_dir} ${fuego_test_tmp}; mkdir -p ${fuego_test_dir}" || abort_job "Could not create ${fuego_test_dir} on $NODE_NAME"
  # note that dump_syslogs (below) creates ${fuego_test_tmp} if needed

# Log test name
  ov_logger "Starting test ${JOB_NAME}"

  dump_syslogs ${fuego_test_tmp} "before"

# flush buffers to physical media and drop filesystem caches to make system load more predictable during test execution
  ov_rootfs_sync

  ov_rootfs_drop_caches
}

function bench_processing {
  firmware
  export GEN_TESTRES_FILE=$GEN_TESTRES_FILE

  echo -e "\n RESULT ANALYSIS \n"

  # Get the test results
  get_testlog $TESTDIR $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR.log

  PYTHON_ARGS="-W ignore::DeprecationWarning -W ignore::UserWarning"

  # parse the test log and create a plot
  # return codes: 0 (everything ok), 1 (problem while parsing, see log), 2 (the results didn't satisfy the threshold)
  run_python $PYTHON_ARGS $FUEGO_CORE/engine/tests/${TESTDIR}/parser.py && rc=0 || rc=$?

  if [ $rc -eq 0 ] || [ $rc -eq 2 ]; then
    # store results as a json file fro the flot plugin
    run_python $PYTHON_ARGS $FUEGO_CORE/engine/scripts/parser/dataload.py && rc=0 || echo "dataload.py didn't work properly"
    if [ $rc -eq 0 ]; then
        # FIXTHIS: this should not be here
        ln -s "../plot.png" "$LOGDIR/plot.png" || true
    else
        false
    fi
  else
    false
  fi
}

# search in test log for {!JOB_NAME}_FAIL_PATTERN_n fail cases and abort with message {!JOB_NAME}_FAIL_MESSAGE_n if found
function fail_check_cases () {
    testlog="${LOGDIR}/testlog.txt"
    slog_prefix="${LOGDIR}/syslog"

    upName=`echo "${TESTDIR^^}"| tr '.' '_'`
    fcname="${upName}"_FAIL_CASE_COUNT

    fcc="${!fcname}"

    if [ -z "$fcc" ]; then
        return 0
    fi

    echo "Going to check $fcc fail cases for $JOB_NAME"

    fcc=`expr $fcc - 1`

    for n in `seq 0 $fcc`
    do
        fpvarname="${upName}"_FAIL_PATTERN_"${n}"
        fpvarmsg="${upName}"_FAIL_MESSAGE_"${n}"
        fpvarslog="${upName}"_FAIL_"${n}"_SYSLOG

        fptemplate="${!fpvarname}"
        fpmessage="${!fpvarmsg}"
        fpslog="${!fpvarslog}"

        if [ ! -z "$fpslog" ]
        then

            if diff -ua ${slog_prefix}.before ${slog_prefix}.after | grep -vEf "$FUEGO_CORE/engine/scripts/syslog.ignore" | grep -E -e $fptemplate;
            then
                echo "Detected fail message in syslog diff: $fpmessage"
            else
                continue
            fi
        fi

        if grep -e "$fptemplate" $testlog ;
        then
            echo "Detected fail message in $testlog: $fpmessage"
        fi
    done
}

# $@ are process names to kill on the target
function kill_procs {
    ov_rootfs_kill "$@"
}

# $1 is the function to call, $2... have arguments to the function
function call_if_present {
    if declare -f -F $1 >/dev/null ; then
        $@ ;
    else
        return 0
    fi
}

# create the run.json file
# this routine uses a whole lot of variables...
function make_run_file {
    # calculate duration
    end_arr=( $(date +"%s %N") )
    # get end time in milliseconds since the epoch
    FUEGO_END_TIME="${end_arr[0]}${end_arr[1]:0:3}"
    DURATION=$(( $FUEGO_END_TIME - $FUEGO_START_TIME ))

    if [ "$FUEGO_RESULT" = "0" ] ; then
        FUEGO_RESULT_STR="SUCCESS"
    else
        FUEGO_RESULT_STR="FAILURE"
    fi

    cat > $LOGDIR/run.json <<RUN_JSON_TEMPLATE
{
    "built_on": "$NODE_NAME",
    "cause": "unknown",
    "description": "unknown",
    "device": "$NODE_NAME",
    "duration": $DURATION,
    "files": [
        "devlog.txt",
        "syslog.before.txt",
        "syslog.after.txt",
        "testlog.txt",
        "consolelog.txt"
    ],
    "host": "$FUEGO_HOST",
    "keep_log": "true",
    "num": "$BUILD_NUMBER",
    "reboot": "$Reboot",
    "rebuild": "$Rebuild",
    "result": "$FUEGO_RESULT_STR",
    "start_time": $FUEGO_START_TIME,
    "target": "$NODE_NAME",
    "target_precleanup": "$Target_PreCleanup",
    "test_name": "$TESTDIR",
    "testplan": "$TESTPLAN",
    "timestamp": "$BUILD_TIMESTAMP",
    "workspace": "$WORKSPACE"
}
RUN_JSON_TEMPLATE
}

# capture SIGTERM during post_test
# if Jenkins is trying to abort us, it sends SIGTERM multiple times
# but we'd rather finish up post_test than stop
function post_term_handler {
  echo "Received SIGTERM during post_test - ignoring it"
}

# $1 - $TESTDIR
function post_test {
  echo "##### doing fuego phase: post_test ########"
  # reset the signal handler to avoid an infinite loop
  trap post_term_handler SIGTERM
  trap - SIGHUP SIGALRM SIGINT ERR EXIT

  export LOGDIR="$FUEGO_RW/logs/$TESTDIR/${NODE_NAME}.${TESTSPEC}.${BUILD_NUMBER}.${BUILD_ID}"

  # source generated prolog.sh file since post_test is called separately
  source $LOGDIR/prolog.sh
  export SSHPASS=$PASSWORD

  is_empty $1

  # call test_cleanup if defined
  # this is for killing runaway processes, and clearing out
  # anything outside the test directories
  call_if_present test_cleanup

  local fuego_test_dir=${BOARD_TESTDIR}/fuego.$1
  local fuego_test_tmp=${FUEGO_TARGET_TMP:-/tmp}/fuego.$1

  # log test completion message.
  ov_logger "Test $1 is finished"

  # Syslog dump
  dump_syslogs ${fuego_test_tmp} "after"

  # Get syslogs
  set +e
  get ${fuego_test_tmp}/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.before ${LOGDIR}/syslog.before.txt
  if [ $? -ne 0 ] ; then
     echo "Fuego error: Can't read 'before' system log, possibly because /tmp was cleared on boot"
     echo "Consider setting FUEGO_TARGET_TMP in your board file to a directory on target that won't get cleared on boot"
     touch ${LOGDIR}/syslog.before.txt
  fi

  get ${fuego_test_tmp}/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.after ${LOGDIR}/syslog.after.txt

  # make a convenience link to the Jenkins console log, if the log doesn't exist
  # this code assumes that if consolelog.txt doesn't exist, this was a Jenkins job build,
  # and the console log is over in the jenkins build directory.
  if [ ! -e $LOGDIR/consolelog.txt ] ; then
     ln -s "/var/lib/jenkins/jobs/${JOB_NAME}/builds/${BUILD_NUMBER}/log" $LOGDIR/consolelog.txt
  fi
  set -e

  # Remove work and log dirs
  [ "$Target_PostCleanup" = "true" ] && target_cleanup $1 || true

  # Syslog comparison
  # FIXTHIS: should affect FUEGO_RESULT
  syslog_cmp

  # FIXTHIS: should affect FUEGO_RESULT
  fail_check_cases  || true

  # create functional result file
  # don't freak out if the parsing doesn't happen
  set +e
  PYTHON_ARGS="-W ignore::DeprecationWarning -W ignore::UserWarning"
  if startswith $TESTDIR "Functional." ; then
      if [ -e "$TEST_HOME/parser.py" ] ; then
         # FIXTHIS: this interface has changed
         run_python $PYTHON_ARGS "$TEST_HOME/parser.py" -t $TESTDIR -b $NODE_NAME -j $JOB_NAME -n $BUILD_NUMBER -s $BUILD_TIMESTAMP -r $FUEGO_RESULT
      fi
  fi
  set -e

  # create run.json file
  make_run_file
}

function target_cleanup {
  local fuego_test_tmp=${FUEGO_TARGET_TMP:-/tmp}/fuego.$1

  cmd "rm -rf $BOARD_TESTDIR/fuego.$1 ${fuego_test_tmp}"
}

# Reboot the target and wait until we reconnect to it
#
# Usage: target_reboot MAX_REBOOT_RETRIES
#
# [Note] retries are used instead of a simple timeout because
# 'cmd' may have an underlying timeout (e.g.: ConnectTimeout=15
# for ssh).
function target_reboot {
  ov_rootfs_reboot
  # give time for reboot to turn off networking, before waiting
  # for target come back up
  sleep 10

  # set +e because cmd will fail during the boot process
  set +e
  retries=0
  while [ $retries -le $1 ]
  do
    cmd "true"
    if [ $? -eq 0 ]; then return 0; fi
    sleep 1
    retries=$((retries+1));
  done
  set -e
  abort_job "FAIL: reboot retries exhausted\n"
}

# $1 - tarball template
function build_cleanup {
 rm -rf ${1}-${PLATFORM}
}

# sets $FUEGO_RESULT
function log_compare {
# 1 - $TESTDIR, 2 - number of results, 3 - Regex, 4 - n or p (i.e. negative or positive)

  if [ ! $FUEGO_RESULT="0" ] ; then
	  return $FUEGO_RESULT
  fi

  cd ${LOGDIR}
  LOGFILE="testlog.txt"
  PARSED_LOGFILE="testlog.${4}.txt"

  if [ -f $LOGFILE ]; then
    current_count=`cat $LOGFILE | grep -E "${3}" 2>&1 | wc -l`
    if [ $current_count -eq $2 ] ; then
      FUEGO_RESULT=0
      # FIXTHIS: make this work and support both the p and n log files
      # cat $LOGFILE | grep -E "${3}" | tee "$PARSED_LOGFILE"
      # local TMP_P=`diff -u ${FUEGO_CORE}/engine/tests/${TESTDIR}/${1}_${4}.log "$PARSED_LOGFILE" 2>&1`
      # if [ $? -ne 0 ];then
      #   echo -e "\nFuego error reason: Unexpected test log output:\n$TMP_P\n"
      #   check_create_functional_logrun "test error"
      #   FUEGO_RESULT=1
      # else
      #   check_create_functional_logrun "passed"
      #   FUEGO_RESULT=0
      # fi
    else
      echo -e "\nFuego error reason: Mismatch in expected ($2) and actual ($current_count) pos/neg ($4) results. (pattern: $3)\n"
      check_create_functional_logrun "failed"
      FUEGO_RESULT=1
    fi
  else
    echo -e "\nFuego error reason: '$LOGDIR/$LOGFILE' is missing.\n"
    check_create_functional_logrun "test error"
    FUEGO_RESULT=1
  fi

  cd -
  return $FUEGO_RESULT
}

function get_testlog {
# $1 - testdir,  $2 - full path to logfile
# XXX: It will be unified
  if [ -n "$2" ]; then
    get ${2} ${LOGDIR}/testlog.txt
  else
    get $BOARD_TESTDIR/fuego.$1/$1.log ${LOGDIR}/testlog.txt
  fi;
}

function syslog_cmp {
  PREFIX="$LOGDIR/syslog"
  rc=0
  if [ -f ${PREFIX}.before ]; then
    if diff -ua ${PREFIX}.before ${PREFIX}.after | grep -vEf "$FUEGO_CORE/engine/scripts/syslog.ignore" | grep -E -e '\.(Bug:|Oops)'; then
      rc=1
    fi
  # else # special case for "reboot" test
    # if grep -vE -e '\.(info|notice|debug|warn)|Boot Reason: Warm' -f "$FUEGO_CORE/engine/scripts/syslog.ignore" ${PREFIX}.after; then
    #   rc=1
    # fi
  fi
  [ $rc -eq 1 ] && echo -e "\nFuego error reason: Unexpected syslog messages.\n"
  return $rc
}

# check is variable is set and fail if otherwise
function check_capability () {
    varname=CAP_$1
    if [ -z "${!varname}" ]
    then
        abort_job "CAP_$1 is not defined. Make sure you use correct overlay with required specs for this test/benchmark"
    fi
}

# check that a process ($1 is the process name) is running
function check_process_is_running {
    pgrep $1 && rc=0 || rc=$?
    if [ $rc -eq 1 ]; then
        abort_job "process $1 is not running"
    fi
}

function hd_test_mount_prepare () {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ]
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV || /bin/true"
        cmd "mount $HD_MOUNT_BLOCKDEV $HD_MOUNT_POINT"
    fi

    cmd "mkdir -p $HD_MOUNT_POINT/fuego.$TESTDIR"
}

function hd_test_clean_umount() {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    cmd "rm -rf $HD_MOUNT_POINT/fuego.$TESTDIR"

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ]
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV"
    fi
}

# check for a program or file on the target, and set a variable if it's present
# $1 - file, dir or program on target
# $2 - variable to set during the build
# $3 - (optional) set of paths to look for on target
#      if $3 is not specified, a find is done from the root
#      this requires the 'find' command on the target
function is_on_target {
 tmpfile=$(mktemp /tmp/found_loc.XXXXXX)
 cmd "touch $tmpfile"
 if [ -z "$3" ] ; then
    safe_cmd "find / -name \"$1\" | head -n 1 >$tmpfile"
 else
    # split search path on colon
    for d in $(echo "$3" | tr ":" "\n") ; do
       # execute a command on the target to detect $d/$1
       cmd "if [ -z \"\$(cat $tmpfile)\" -a -e \"$d/$1\" ] ; then echo \"$d/$1\" >$tmpfile ; fi"
    done
 fi
 get $tmpfile $tmpfile
 LOCATION=$(cat $tmpfile)
 export $2=$LOCATION
 cmd "rm $tmpfile"
 rm -f $tmpfile # -f for tests running on the host
}
